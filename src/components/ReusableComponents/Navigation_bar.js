import React from 'react'
import { Link } from 'react-router-dom'

const Navigation_bar = ({slide}) => {
    return (
        <div className="navigationbar">
            <i className="material-icons menu1" style={{cursor:'pointer'}} onClick={slide}>menu</i>
            <Link className="logo-container" to="/">
                <img
                    className="logo"
                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHIAAABRCAMAAAA5OwLXAAAAQlBMVEX////9+/n79e7369324s/z2L67ubd2dnZKR0YhHRnW1dTxz7Dxt4jvtoYBAQGTkpLvxJ3ClW+PblPorXjno2Xll1GL/vNhAAAGYklEQVR42uxXiaKlKgyzgArWjqj//69v9JQIng3evmXm7kBIW0Lp/sffDmTsCfMHchjn+r53JwcNow8KP07dr8HgP2EwjucDMs8iMw/+R4GwXKw2Ap8DsPz4gLCKyJxBZAu3MQvpSv0M9L+actszQhUr+3Yb5e0jxXEGIrVSQuIMxlzoehc6HAu5fKz5dZSr3Mk0pbLfOY+EsmQj3a+hDMoo+u8SLM86w9RRzCkj/QrKLaOYZV/XXSQRi6zeh4LTWI2DjrE1lH7M8WCU86M3QziW3XbBLlxnpiVjHaMU+e5rKItjba44sumMVy2bapBH5Ox4ySxFzrGVkljSCj3lwV+zAiljhBJTmEZKiJSDkTwWNpyERCqmr6DTvfaNlH2aeFrXhOjZLOIHJbYTdiVjFtRsC6UWvBYeIWOL7uaMukZuCqjvY4/WpozYJkr7WEC3agJEHn+b5QCWJF/kmCimlDRR9nmNEOI6nmnmn4jMnOpjOOOqGeaOWA0kUgMlxXRZmVcjiB6fFPQzCB6JgNeK2DbKvARSKsObbfvTjlFtlGqXGwwv7Foh3OWU/nmy/j1IqlfK/D1SE2V+03pQvlsB7ujO4kuWYFsodY4rKMd3rczlA/TwkaS5khL1B8rwjVLjiuxx2oFpoNQ6t3UqYfZOj/WssK25VEr6Rjml4kmqKKpMbq9YV1U+LolEiTK6rs+U4wKgAKsoecbwu2G6WiswM2yywgrMnEBPv+JXU+id+wiq/BrxJq6auWgATpenaTM80X0P1911gBTXYEBSNyZwg0qVHae5titu6MNBzf3yQs+TdyJwwNrLy5UzfN6Vu1tz40ouQB6iTaVKgzVMEVlvsqac0EBApaTQAuJqe59YWJgJOEjofSQSfKCUWXb3XKES0ZLrnr3a1YVFsu3TtAm61zvg9HeM75tKtHgWMn3Rp9IQ1ptEQBDZym691xlamUv+UEFh0Yh7TtiUcIk2Up3K4h3Fls4XQvB4cIoMw+KxhxdiCDJNpcrOSV5+zNu2imTP9/QiTMfB3Cl7+GyFSoRWIYqMck3m8P7Iw6hjNSVxXonlgVsDcvvW2CiiN6gJrHKCojhw+bt9//Bo7nEfVahEOuBbuVzZyiMjb6zUpkmRKigxK+bhTE/22+Ne3jTmFHWv98jS4BUvphHZqEWjL4Y4jeGiFH6gh44clhW2awIZxxxPcO/MsY1pGJZl+aWd8lC2IAWBKHGTY5VV0P//qys494XZnON5wbEFWrjhiy++4Z9Ipd8E0//87bHcTE3rTQCh5K0J0chFzUjkYvLAjqUVXFrQpmMvkplIZ81Nnx6B3oV3DMNMDV5hGeaR4nAbMOIA5AQtHRia0zyDr6waK/sycDPlSJMFVazabX2m2wXTLv+0LOFYOpRI4bUqjBQL1T3PZGK3lR1qny2FmbXyWWpl7iZbiEthJTwsN34sR3IFD0dGhtBK7clyRGfNoL7SZ8vNqAv6nKbYVLBCO6EFf1oq81uXcnc5fEIk0yO5ugwmVukuFTYfXXbStoTX2pyHmG39wFs7lorFvKClDaghInqyo6c8DIM4Uh6W513A5Lcllx4p3ILSE7stN5MHMuF9DYetFOKYNeyJRAgvZKbR7GEubHycUVpmZLqi6IqJxOLvs2Th/i/CVdudj8YN7bWjzK0epBd+Upr7++5OkX//N+X/sCr3UvSjqJtwKb0R2rIeRO61EtyV6QgdwV3lpXSBE/vEgNFLs4g0+4EFyUSOxZGcaJajceJZa4jhoHG+SIIHmkH6KvmgUqNLTzMHhAPLLGCfLOsszdQRthFa9bBwidnEtZUPlqujRU+s0gNBBrZqdRsOqGOWU66yZB7HsqqdG1x3GuT+1qFVY+K2tLLsbUK0Kz3pLhyru3QfCL7g1TyzALn5bDmQETHFENRhRHxbvnd5uS9MViA2Qg8mTDLqfo2VQI10xtOy7xFi3e3henbZKGnH5sOy5DUSUhNTXQh67/Ke5HOwTMzdzg92uarSRT8w2IFmVekuWxeV0sf3Wo7Xa5mwdqD311L75anUbvdl+bQNmIgi5WXJHJg2kPoDlnltxtbTPTHuwfaJj4R/tDyxnz1PWQ5YWwqgJAEgjX7Asoka52bxq8tbGHwsFRBF85gui5yFWepE6j+rmbzOSu+H17bp2IradGqhR3ir9B77P//zW/kWD7rKXhWH+pQAAAAASUVORK5CYII="
                    alt="logo"
                />
                <div className="navigationbar-container">
                    <span className="title" style={{color:'black'}}>FLORA COLLEGE OF ARCHITECTURE</span>
                    <div className="sub-title">Architecture is an art we live in</div>
                </div>
            </Link>
            <div className="navbar">
                <Link className="dropbtn" to="/">HOME</Link>
                <div className="dropdown">
                    <button className="dropbtn">ABOUT</button>
                    <div className="dropdown-content">
                        <Link to="/about">About FCOA</Link>
                        <Link to="/vision">Vision</Link>
                        <Link to="/mission">Mission</Link>
                        <Link to="/notefromdirector">Note from Director</Link>
                        <Link to="/principlemessage">Principals Message</Link>
                        <Link to="/governingcounsil">Governing Council</Link>
                    </div>
                </div>
                <div className="dropdown">
                    <button className="dropbtn">FACULTY</button>
                    <div className="dropdown-content">
                        <Link to="/faculty">Faculty</Link>
                        <Link to="/staffactivity">Staff Activity</Link>
                        <Link to="/adminstaff">Admin Staff</Link>
                    </div>
                </div>
                <div className="dropdown">
                    <button className="dropbtn">COURSES</button>
                    <div className="dropdown-content">
                        <Link to="/barch">B. Arch.</Link>
                        <Link to="/prospectingstudent">Prospecting Students</Link>
                    </div>
                </div>
                <div className="dropdown">
                    <button className="dropbtn">ADMISSION</button>
                    <div className="dropdown-content">
                        <Link to="/overview">Admission</Link>
                        <Link to="/schorlship">Grants &amp; Scholarships</Link>
                    </div>
                </div>
                <div className="dropdown">
                    <button className="dropbtn">ACTIVITIES</button>
                    <div className="dropdown-content">
                        <Link to="/events">Events</Link>
                        <Link to="/workshop">Workshops</Link>
                        <Link to="/studycase">Study tours &amp; Case Studies</Link>
                        <Link to="/guestlecture">Guest Lectures</Link>
                        <Link to="/competition">Competitions</Link>
                        <Link to="/annual">Annual College Exhibition</Link>
                    </div>
                </div>
                <div className="dropdown">
                    <button className="dropbtn">STUDENT CORNER</button>
                    <div className="dropdown-content">
                        <Link to="/academicwork">Academic Work</Link>
                        <Link to="/ict">ICT</Link><Link to="/hostel">Hostel</Link>
                        <Link to="/library">Library</Link>
                        <Link to="/sport">Sport</Link>
                        <Link to="/transport">Transport</Link>
                        <Link to="/computerlab">Computer Lab</Link>
                        <Link to="/materiallibrary">Material Laboratory</Link>
                    </div>
                </div>
                <div className="dropdown">
                    <button className="dropbtn">COMMITTEE</button>
                    <div className="dropdown-content1">
                        <Link to="/antiraggingcomitee">Anti-Ragging Committee</Link>
                        <Link to="/vishakacommitee">Vishaka Committee</Link>
                        <Link to="/internalcommitee">Internal Compliance Committee</Link>
                        <Link to="/scstcommitee">SC/ST Committee</Link>
                        <Link to="/grivance">Grivance Redressal Committee </Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Navigation_bar
