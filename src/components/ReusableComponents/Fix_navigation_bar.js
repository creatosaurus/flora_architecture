import React from 'react'
import { Link } from 'react-router-dom'

const Fix_navigation_bar = () => {
    return (
        <nav class="fix_navigation_bar">
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <i class="material-icons" style={{marginRight:10, width:25}}>email</i>
                <span>flora.architecture@flora.ac.in</span>
                <i class="material-icons" style={{marginLeft:10, marginRight:10, width:25}}>phone</i>
                <span>9890-673-701</span>
            </div>
            <Link to="#admission" style={{marginLeft:10}}>Admission open 2020-2021</Link>
            <div>
                <Link to="/news">News</Link>
                <Link to="/events">Events</Link>
                <Link to="/barch">Admission</Link>
                <Link to="/contact">Enquiry</Link>
            </div>
        </nav>

    )
}

export default Fix_navigation_bar
