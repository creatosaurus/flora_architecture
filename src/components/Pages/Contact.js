import React,{useState} from 'react'
import Social from '../../assets/social.svg'
import Info from '../../assets/commerce-and-shopping.svg'
import Navigationbar from '../ReusableComponents/Navigation_bar'
import Footer from '../ReusableComponents/Footer'
import emailjs from 'emailjs-com';
import Fix_navigation_bar from '../ReusableComponents/Fix_navigation_bar'
import Button from '../ReusableComponents/Button'

function Contact() {
    const [flag, setflag] = useState(false)
    const [name, setname] = useState("")
    const [email, setemail] = useState("")
    const [city, setcity] = useState("")
    const [contact, setcontact] = useState("")

    const showForm = () => {
        setflag(!flag)
    }

    const sendEmail = () => {
        
        var template_params = {
            "name": name,
            "email": email,
            "city": city,
            "contact": contact
        }
        emailjs.send('default_service', 'template_QcFeJJkU', template_params, 'user_pMyeJVJzDOA6CDIxE2aM8')
            .then((result) => {
                alert('Form Submitted')
            }, (error) => {
                alert('Please try again')
            });
    }

    
    return (
        <div>

    <Fix_navigation_bar />
    <Navigationbar/>
        <div className="main-container">
               {
                    flag === false ? <div class="circle" onClick={showForm}>
                        <img src={Info} alt="info" />
                    </div> : <div className="form">
                            <div className="head">
                                <div></div>
                                <div>Admission Inquiry</div>
                                <div onClick={showForm} style={{ cursor: 'pointer' }}>X</div>
                            </div>
                            <div className="form-content">
                                <div className="icon-container">
                                    <span className="icon">
                                        <img src={Social} alt="social" />
                                    </span>
                                    <span className="text-content">
                                        Hello! Welcome to Flora college
                              </span>
                                </div>
                                <div className="icon-container1">
                                    <span className="icon">
                                        <img src={Social} alt="social" />
                                    </span>
                                    <span className="text-content" style={{ display: 'flex', flexDirection: 'column' }}>
                                        Please Tell a bit about yourself.
                              <div style={{ marginTop: 15, width: '70%' }}>
                                            <div>Your Name</div>
                                            <input onChange={(e)=>setname(e.target.value)} type="text" placeholder="full name" />
                                        </div>
                                        <div style={{ marginTop: 15, width: '70%' }}>
                                            <div>Your E-mail Address</div>
                                            <input onChange={(e)=>setemail(e.target.value)} type="text" placeholder="E-mail" />
                                        </div>
                                        <div style={{ marginTop: 15, width: '70%' }}>
                                            <div>Contact Number</div>
                                            <input onChange={(e)=>setcontact(e.target.value)} type="text" placeholder="Contact Number" />
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div onClick={sendEmail} className="submit">
                                SUBMIT
                   </div>
                        </div>
                }
            <div className="contact-us-header-container">
            <div className="contact-us-header">
                Contact
            </div>
           
            </div>
            <div className="getin-touch-container">
                <div className="getin-touch-border"></div>
                <div className="getin-touch-header">GET IN TOUCH</div>
            </div>
            <div className="contact-form">
                <div className="name-container">
                    <div className="name-header">
                    NAME*
                    </div>
                    <input onChange={(e)=>setname(e.target.value)} className="name-input" type="text" placeholder="Enter your name" />
                    
                </div>
                <div className="name-container">
                <div className="name-header">
                EMAIL*
                    </div>
                    <input onChange={(e)=>setemail(e.target.value)} className="name-input" type="text" placeholder="Enter E-mail address" />
                  
                </div>
                <div className="name-container">
                <div className="name-header">
                Purpose*
                    </div>
                    <input onChange={(e)=>setemail(e.target.value)} className="name-input" type="text" placeholder="Enter Purpose" />
                  
                </div>
              
            </div>

            <div className="contact-form">
                <div className="name-container">
                    <div className="name-header">
                    CITY
                    </div>
                    <input onChange={(e)=>setcity(e.target.value)} className="name-input" type="text" placeholder="Enter your city" />
                    
                </div>
                <div className="name-container">
                <div className="contact-header">
                CONTACT NUMBER*
                    </div>
                    <input onChange={(e)=>setcontact(e.target.value)} className="name-input" type="text" placeholder="Enter your contact number" />
                  
                </div>
                <div style={{border:'white'}} className="name-container">
                </div>
              
            </div>
            <div  className="submit-container">
            <div style={{marginBottom:20}} className="button">
            <div style={{color:'black'}} >Submit</div>
           
        </div>  
            </div>
           </div>
           <Footer/>
        
        </div>
    )
}

export default Contact
