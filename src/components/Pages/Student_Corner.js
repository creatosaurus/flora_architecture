import React, { useState } from 'react'
import Navigationbar from '../ReusableComponents/Navigation_bar'
import Footer from '../ReusableComponents/Footer'
import { Link } from 'react-router-dom'
import Info from '../../assets/commerce-and-shopping.svg'
import Social from '../../assets/social.svg'
import emailjs from 'emailjs-com';
import Slider from '../ReusableComponents/Slider'
import Fix_navigation_bar from '../ReusableComponents/Fix_navigation_bar'
import Firstpdf from '../../assets/studentpdf.png'
import Firstpdf2 from '../../assets/studentpdf2.png'
import Firstpdf3 from '../../assets/studentpdf3.png'
import Firstpdf4 from '../../assets/studentpdf4.png'
import Firstpdf5 from '../../assets/studentpdf5.png'
import Firstpdf6 from '../../assets/studentpdf6.png'
import Firstpdf7 from '../../assets/studentpdf7.png'
import Firstpdf8 from '../../assets/studentpdf8.png'
import Firstpdf9 from '../../assets/studentpdf9.png'
import Firstpdf10 from '../../assets/studentpdf10.png'
import pdf from '../../assets/studentpdf.pdf'
import pdf2 from '../../assets/studentpdf2.pdf'
import pdf3 from '../../assets/studentpdf3.pdf'
import pdf4 from '../../assets/studentpdf4.pdf'
import pdf5 from '../../assets/studentpdf5.pdf'
import pdf6 from '../../assets/studentpdf6.pdf'
import pdf7 from '../../assets/studentpdf7.pdf'
import pdf8 from '../../assets/studentpdf8.pdf'
import pdf9 from '../../assets/studentpdf9.pdf'
import pdf10 from '../../assets/studentpdf10.pdf'


function Student_Corner() {
    const [flag, setflag] = useState(false)
    const [name, setname] = useState("")
    const [email, setemail] = useState("")
    const [contact, setcontact] = useState("")
    const [menu, setMenu] = useState(false)

    const showMenuBar = () => {
        setMenu(!menu)
    }


    const sendEmail = () => {

        var template_params = {
            "name": name,
            "email": email,
            "contact": contact
        }
        emailjs.send('default_service', 'template_QcFeJJkU', template_params, 'user_pMyeJVJzDOA6CDIxE2aM8')
            .then((result) => {
                alert('Form Submitted')
            }, (error) => {
                alert('Please try again')
            });
    }


    const showForm = () => {
        setflag(!flag)
    }

    return (
        <div>
            {
                menu === false ? null
                    : <Slider slide={showMenuBar} />
            }
            <Fix_navigation_bar />
            <Navigationbar slide={showMenuBar} />
            <div class="student-corner-main-container">
                {
                    flag === false ? <div class="circle" onClick={showForm}>
                        <img src={Info} alt="info" />
                    </div> : <div className="form">
                            <div className="head">
                                <div></div>
                                <div>Admission Inquiry</div>
                                <div onClick={showForm} style={{ cursor: 'pointer' }}>X</div>
                            </div>
                            <div className="form-content">
                                <div className="icon-container">
                                    <span className="icon">
                                        <img src={Social} alt="social" />
                                    </span>
                                    <span className="text-content">
                                        Hello! Welcome to Flora college
                              </span>
                                </div>
                                <div className="icon-container1">
                                    <span className="icon">
                                        <img src={Social} alt="social" />
                                    </span>
                                    <span className="text-content" style={{ display: 'flex', flexDirection: 'column' }}>
                                        Please Tell a bit about yourself.
                              <div style={{ marginTop: 15, width: '70%' }}>
                                            <div>Your Name</div>
                                            <input onChange={(e) => setname(e.target.value)} type="text" placeholder="full name" />
                                        </div>
                                        <div style={{ marginTop: 15, width: '70%' }}>
                                            <div>Your E-mail Address</div>
                                            <input onChange={(e) => setemail(e.target.value)} type="text" placeholder="E-mail" />
                                        </div>
                                        <div style={{ marginTop: 15, width: '70%' }}>
                                            <div>Contact Number</div>
                                            <input onChange={(e) => setcontact(e.target.value)} type="text" placeholder="Contact Number" />
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div onClick={sendEmail} className="submit">
                                SUBMIT
                   </div>
                        </div>
                }

                <div class="student-corner-sub-nav">
                    <Link to="/academicwork" className="active student-corner-sub-nav-item">Academic Work</Link>
                    <Link to="/ict" className="inactive student-corner-sub-nav-item">ICT</Link>
                    <Link to="/hostel" className="inactive student-corner-sub-nav-item">Hostel</Link>
                    <Link to="/library" className="inactive student-corner-sub-nav-item">Library</Link>
                    <Link to="/sport" className="inactive student-corner-sub-nav-item">Sports</Link>
                    <Link to="/transport" className="inactive student-corner-sub-nav-item">Transport</Link>
                    <Link to="/computerlab" className="inactive student-corner-sub-nav-item">Computer lab</Link>
                    <Link to="/materiallibrary" className="inactive student-corner-sub-nav-item">Material Library</Link>
                </div>
                <div className="student-corner-container">
                    <div className="student-corner-container-header">
                        <div>
                            Academic Work
                    </div>
                        <div className="student-corner-container-border">

                        </div>
                    </div>
                    <div class="student-corner-container-paragraph">
                        At FCOA, we understand that to inculcate creativity, we have to provide adequate facilities to the teaching faculty and the young students. We have made Wi-Fi enabled creative room to relax and socialize. The room is equipped with a computer, printer, and a projector. Tea center and a few board games are provided for students and faculty when they want to relax a bit from work.
                </div>
                    <h2 style={{ marginTop: 10, marginLeft: 20 }}>1. Fourth year</h2>
                    <div style={{ paddingBottom: 30 }} class="student-corner-ICT-card-container">
                        <div class="student-corner-ICT-card">
                            <img src={Firstpdf} alt="" className="pdfImage" />
                            <button className="pdfbutton"><a style={{ color: '#fff' }} href={pdf} target="_blank">View PDF</a></button>
                        </div>
                        <div class="student-corner-ICT-card">
                            <img src={Firstpdf2} alt="" className="pdfImage" />
                            <button className="pdfbutton"><a style={{ color: '#fff' }} href={pdf2} target="_blank">View PDF</a></button>
                        </div>
                        <div class="student-corner-ICT-card">
                            <img src={Firstpdf3} alt="" className="pdfImage" />
                            <button className="pdfbutton"><a style={{ color: '#fff' }} href={pdf3} target="_blank">View PDF</a></button>
                        </div>
                        <div class="student-corner-ICT-card">
                            <img src={Firstpdf4} alt="" className="pdfImage" />
                            <button className="pdfbutton"><a style={{ color: '#fff' }} href={pdf4} target="_blank">View PDF</a></button>
                        </div>
                        <div class="student-corner-ICT-card">
                            <img src={Firstpdf5} alt="" className="pdfImage" />
                            <button className="pdfbutton"><a style={{ color: '#fff' }} href={pdf5} target="_blank">View PDF</a></button>
                        </div>
                        <div class="student-corner-ICT-card">
                            <img src={Firstpdf6} alt="" className="pdfImage" />
                            <button className="pdfbutton"><a style={{ color: '#fff' }} href={pdf6} target="_blank">View PDF</a></button>
                        </div>
                    </div>
                    <h2 style={{ marginTop: 10, marginLeft: 20 }}>2. Third year</h2>
                    <div class="student-corner-ICT-card-container" style={{ marginBottom: 20 }}>
                        <div class="student-corner-ICT-card">
                            <img src={Firstpdf7} alt="" className="pdfImage" />
                            <button className="pdfbutton"><a style={{ color: '#fff' }} href={pdf7} target="_blank">View PDF</a></button>
                        </div>
                        <div class="student-corner-ICT-card">
                            <img src={Firstpdf8} alt="" className="pdfImage" />
                            <button className="pdfbutton"><a style={{ color: '#fff' }} href={pdf8} target="_blank">View PDF</a></button>
                        </div>
                        <div class="student-corner-ICT-card">
                            <img src={Firstpdf9} alt="" className="pdfImage" />
                            <button className="pdfbutton"><a style={{ color: '#fff' }} href={pdf9} target="_blank">View PDF</a></button>
                        </div>
                        <div class="student-corner-ICT-card">
                            <img src={Firstpdf10} alt="" className="pdfImage" />
                            <button className="pdfbutton"><a style={{ color: '#fff' }} href={pdf10} target="_blank">View PDF</a></button>
                        </div>
                    </div>
                </div>

            </div>
            <Footer />


        </div>

    )
}

export default Student_Corner
