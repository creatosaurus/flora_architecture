import React, { useState } from 'react'
import Navigationbar from '../ReusableComponents/Navigation_bar'
import Footer from '../ReusableComponents/Footer'
import Info from '../../assets/commerce-and-shopping.svg'
import Social from '../../assets/social.svg'
import { Link } from 'react-router-dom'
import emailjs from 'emailjs-com';
import Fix_navigation_bar from '../ReusableComponents/Fix_navigation_bar'
import Slider from '../ReusableComponents/Slider'


const Governingcounsil = () => {
    const [flag, setflag] = useState(false)
    const [name, setname] = useState("")
    const [email, setemail] = useState("")
    const [contact, setcontact] = useState("")
    const [menu, setmenu] = useState(false)

    const showMenuBar = () => {
        setmenu(!menu)
    }


    const sendEmail = () => {

        var template_params = {
            "name": name,
            "email": email,
            "contact": contact
        }
        emailjs.send('default_service', 'template_QcFeJJkU', template_params, 'user_pMyeJVJzDOA6CDIxE2aM8')
            .then((result) => {
                alert('Form Submitted')
            }, (error) => {
                alert('Please try again')
            });
    }

    const showForm = () => {
        setflag(!flag)
    }

    return (
        <div>
            {
                menu === false ? null
                    : <Slider slide={showMenuBar} />
            }
            {
                flag === false ? <div class="circle" onClick={showForm}>
                    <img src={Info} alt="info" />
                </div> : <div className="form">
                        <div className="head">
                            <div></div>
                            <div>Admission Inquiry</div>
                            <div onClick={showForm} style={{ cursor: 'pointer' }}>X</div>
                        </div>
                        <div className="form-content">
                            <div className="icon-container">
                                <span className="icon">
                                    <img src={Social} alt="social" />
                                </span>
                                <span className="text-content">
                                    Hello! Welcome to Flora college
                              </span>
                            </div>
                            <div className="icon-container1">
                                <span className="icon">
                                    <img src={Social} alt="social" />
                                </span>
                                <span className="text-content" style={{ display: 'flex', flexDirection: 'column' }}>
                                    Please Tell a bit about yourself.
                              <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Your Name</div>
                                        <input onChange={(e) => setname(e.target.value)} type="text" placeholder="full name" />
                                    </div>
                                    <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Your E-mail Address</div>
                                        <input onChange={(e) => setemail(e.target.value)} type="text" placeholder="E-mail" />
                                    </div>
                                    <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Contact Number</div>
                                        <input onChange={(e) => setcontact(e.target.value)} type="text" placeholder="Contact Number" />
                                    </div>
                                </span>
                            </div>
                        </div>
                        <div onClick={sendEmail} className="submit">
                            SUBMIT
                            </div>
                    </div>
            }

            <Fix_navigation_bar />
            <Navigationbar slide={showMenuBar} />
            <div className="about-sub-nav">
                <Link to="/about" className="inactive about-sub-nav-item">About us</Link>
                <Link to="/vision" className="inactive about-sub-nav-item">Vision</Link>
                <Link to="/mission" className="inactive about-sub-nav-item">Mission</Link>
                <Link to="/notefromdirector" className="inactive about-sub-nav-item">About Director</Link>
                <Link to="/principlemessage" className="inactive about-sub-nav-item">Principal's Message</Link>
                <Link to="/governingcounsil" className="active about-sub-nav-item">Governing Council</Link>
            </div>
            <div className="governingcounsil-conteiner" >
                <div className="governingcounsil-title" >
                    <strong>Governing Council</strong>
                    <div className="about-line"></div>
                </div>
                <table>
                    <tr>
                        <th style={{ textAlign: 'center' }}>Sr.No</th>
                        <th style={{ textAlign: 'center' }}>Name</th>
                        <th style={{ textAlign: 'center' }}>Designation</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Mrs. Madhuri Padalkar</td>
                        <td>Chairperson</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Dr. Atul Padalkar</td>
                        <td>Member</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Mr. Rajaram Padalkar</td>
                        <td>Member</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Mr. Ramesh Padalkar</td>
                        <td>Member</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Ar. Avinash Nawathe</td>
                        <td>Member</td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>Ar. Abhay Purohit</td>
                        <td>Member</td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>Dr. Anurag Kashyap</td>
                        <td>Member</td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>Dr. Anurag Kashyap</td>
                        <td>Member</td>
                    </tr>
                    <tr>
                        <td>9</td>
                        <td>Ar. Ulhas Rane</td>
                        <td>Member</td>
                    </tr>
                    <tr>
                        <td>10</td>
                        <td>Ar. Mahesh Bangad</td>
                        <td>Member</td>
                    </tr>
                    <tr>
                        <td>11</td>
                        <td>Ar. Rahul Patil</td>
                        <td>Member Secretary</td>
                    </tr>
                </table>

            </div>
            <Footer />
        </div>
    )
}

export default Governingcounsil
