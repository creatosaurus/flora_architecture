import React, { useState } from 'react'
import Social from '../../assets/social.svg'
import Info from '../../assets/commerce-and-shopping.svg'
import Navigationbar from '../ReusableComponents/Navigation_bar'
import Fix_navigation_bar from '../ReusableComponents/Fix_navigation_bar'
import Footer from '../ReusableComponents/Footer'
import { Link } from 'react-router-dom'
import emailjs from 'emailjs-com';
import Slider from '../ReusableComponents/Slider'

function Contact() {
    const [flag, setflag] = useState(false)
    const [name, setname] = useState("")
    const [email, setemail] = useState("")
    const [contact, setcontact] = useState("")
    const [menu, setmenu] = useState(false)

    const showMenuBar = () => {
        setmenu(!menu)
    }

    const sendEmail = () => {

        var template_params = {
            "name": name,
            "email": email,
            "contact": contact
        }
        emailjs.send('default_service', 'template_QcFeJJkU', template_params, 'user_pMyeJVJzDOA6CDIxE2aM8')
            .then((result) => {
                alert('Form Submitted')
            }, (error) => {
                alert('Please try again')
            });
    }

    const showForm = () => {
        setflag(!flag)
    }

    return (
        <div>
            {
                menu === false ? null
                    : <Slider slide={showMenuBar} />
            }
            <Fix_navigation_bar />
            <Navigationbar slide={showMenuBar} />
            <div className="overview-sub-nav">
                <Link to="/overview" className="overview-sub-nav-item inactive">Admissions</Link>
                <Link to="/schorlship" className="overview-sub-nav-item active">Scholarship</Link>

            </div>



            <div className="main-container">
                {
                    flag === false ? <div class="circle" onClick={showForm}>
                        <img src={Info} alt="info" />
                    </div> : <div className="form">
                            <div className="head">
                                <div></div>
                                <div>Admission Inquiry</div>
                                <div onClick={showForm} style={{ cursor: 'pointer' }}>X</div>
                            </div>
                            <div className="form-content">
                                <div className="icon-container">
                                    <span className="icon">
                                        <img src={Social} alt="social" />
                                    </span>
                                    <span className="text-content">
                                        Hello! Welcome to Flora college
                              </span>
                                </div>
                                <div className="icon-container1">
                                    <span className="icon">
                                        <img src={Social} alt="social" />
                                    </span>
                                    <span className="text-content" style={{ display: 'flex', flexDirection: 'column' }}>
                                        Please Tell a bit about yourself.
                              <div style={{ marginTop: 15, width: '70%' }}>
                                            <div>Your Name</div>
                                            <input onChange={(e) => setname(e.target.value)} type="text" placeholder="full name" />
                                        </div>
                                        <div style={{ marginTop: 15, width: '70%' }}>
                                            <div>Your E-mail Address</div>
                                            <input onChange={(e) => setemail(e.target.value)} type="text" placeholder="E-mail" />
                                        </div>
                                        <div style={{ marginTop: 15, width: '70%' }}>
                                            <div>Contact Number</div>
                                            <input onChange={(e) => setcontact(e.target.value)} type="text" placeholder="Contact Number" />
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div onClick={sendEmail} className="submit">
                                SUBMIT
                   </div>
                        </div>
                }

                <div className="contact-us-header-container">
                    <div className="contact-us-header">
                        Schorlship
            </div>

                </div>
                <p className="schorlship-paragraph" style={{ margin: 50 }}>
                    The college helps the students to avail of different scholarships and free ships given by the State, and Central Governments. Admitted students receive scholarships / free ships under the scheme of Government of India Post-Matric for Scheduled Caste (SC) students and Tribal Department for scheduled Tribe (ST), Samaj Kalyan for various categories OBC, VJNT, SBC, and SEBCs.. Economically weaker from General category students and minority students also avail scholarships from the Directorate of Technical Education Department. Information on all types of scholarships is provided to students regularly in the office. The disbursement of the scholarships and free ships has been done promptly as soon as the amounts are received by the concerned department. The college follows the norms laid down by the government regarding the scholarship/free-ships.

              </p>
            </div>
            <Footer />

        </div>
    )
}

export default Contact
