import React,{useState} from 'react'
import Navigationbar from '../ReusableComponents/Navigation_bar'
import Footer from '../ReusableComponents/Footer'
import { Link } from 'react-router-dom'
import Info from '../../assets/commerce-and-shopping.svg'
import Social from '../../assets/social.svg'
import emailjs from 'emailjs-com';
import Fix_navigation_bar from '../ReusableComponents/Fix_navigation_bar'
import Slider from '../ReusableComponents/Slider'

function Competition() {
    const [flag, setflag] = useState(false)
    const [name, setname] = useState("")
    const [email, setemail] = useState("")
    const [contact, setcontact] = useState("")
    const [menu, setmenu] = useState(false)

    const showMenuBar = () => {
        setmenu(!menu)
    }

    const sendEmail = () => {
        
        var template_params = {
            "name": name,
            "email": email,
            "contact": contact
        }
        emailjs.send('default_service', 'template_QcFeJJkU', template_params, 'user_pMyeJVJzDOA6CDIxE2aM8')
            .then((result) => {
                alert('Form Submitted')
            }, (error) => {
                alert('Please try again')
            });
    }
  
    const showForm = () => {
        setflag(!flag)
    }

    return (
   <div className="competition-main-container">
       {
                menu === false ? null
                    : <Slider slide={showMenuBar} />
            }
        {
                    flag === false ? <div class="circle" onClick={showForm}>
                        <img src={Info} alt="info" />
                    </div> : <div className="form">
                            <div className="head">
                                <div></div>
                                <div>Admission Inquiry</div>
                                <div onClick={showForm} style={{ cursor: 'pointer' }}>X</div>
                            </div>
                            <div className="form-content">
                                <div className="icon-container">
                                    <span className="icon">
                                        <img src={Social} alt="social" />
                                    </span>
                                    <span className="text-content">
                                        Hello! Welcome to Flora college
                              </span>
                                </div>
                                <div className="icon-container1">
                                    <span className="icon">
                                        <img src={Social} alt="social" />
                                    </span>
                                    <span className="text-content" style={{ display: 'flex', flexDirection: 'column' }}>
                                        Please Tell a bit about yourself.
                              <div style={{ marginTop: 15, width: '70%' }}>
                                            <div>Your Name</div>
                                            <input onChange={(e)=>setname(e.target.value)} type="text" placeholder="full name" />
                                        </div>
                                        <div style={{ marginTop: 15, width: '70%' }}>
                                            <div>Your E-mail Address</div>
                                            <input onChange={(e)=>setemail(e.target.value)} type="text" placeholder="E-mail" />
                                        </div>
                                        <div style={{ marginTop: 15, width: '70%' }}>
                                            <div>Contact Number</div>
                                            <input onChange={(e)=>setcontact(e.target.value)} type="text" placeholder="Contact Number" />
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div onClick={sendEmail} className="submit">
                                SUBMIT
                   </div>
                        </div>
                }
       
          <Fix_navigation_bar />
          <Navigationbar slide={showMenuBar} />
            <div className="competition-sub-nav">
                <Link className="inactive competition-sub-nav-item" to="/events">Events</Link>
                <Link className="inactive competition-sub-nav-item" to="/workshop">Workshops</Link>
                <Link className="inactive competition-sub-nav-item" to="/studycase">Study tours & Case Studies</Link>
                <Link className="inactive competition-sub-nav-item" to="/guestlecture">Guest Lectures</Link>
                <Link className="active competition-sub-nav-item" to="/competition">Competitions</Link>
                <Link className="inactive competition-sub-nav-item" to="/annual">Annual College Exhibition</Link>
            
            </div>
            <div className="competition-container">
                <div className="competition-container-header">
                    <div>
                    Competitions
                    </div>
                    <div className="competition-container-border">
    
                    </div>
                </div>
                <div className="competition-container-paragraph">
                 <strong> Competition:</strong><br/> <strong> 1. Umbrella Painting competition:</strong><br/> Flora second-year student organised the competition for Pune city Architecture students on Umbrella painting in July 2018.
               </div>
            <div class="competition-ICT-card-container">
                        <div class="competition-ICT-card">
                            <img className="competition-image" src={require('../../assets/umbrella 1.jpg')} alt=""/>
                        </div>
                        <div class="competition-ICT-card">
                        <img className="competition-image" src={require('../../assets/umbrella 2.jpg')} alt=""/>
                        
                        </div>
                        
              </div>
            </div>
            <Footer/>
        </div>


        )
}

export default Competition
