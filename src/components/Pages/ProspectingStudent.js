import React,{useState} from 'react'
import Navigationbar from '../ReusableComponents/Navigation_bar'
import Fix_navigation_bar from '../ReusableComponents/Fix_navigation_bar'
import Footer from '../ReusableComponents/Footer'
import { Link } from 'react-router-dom'
import Info from '../../assets/commerce-and-shopping.svg'
import Social from '../../assets/social.svg'
import emailjs from 'emailjs-com';
import Slider from '../ReusableComponents/Slider'


function ProspectingStudent() {
    const [flag, setflag] = useState(false)
    const [name, setname] = useState("")
    const [email, setemail] = useState("")
    const [contact, setcontact] = useState("")
    const [menu, setmenu] = useState(false)

    const showMenuBar = () => {
        setmenu(!menu)
    }

  
    const sendEmail = () => {
        
        var template_params = {
            "name": name,
            "email": email,
            "contact": contact
        }
        emailjs.send('default_service', 'template_QcFeJJkU', template_params, 'user_pMyeJVJzDOA6CDIxE2aM8')
            .then((result) => {
                alert('Form Submitted')
            }, (error) => {
                alert('Please try again')
            });
    }
  

    const showForm = () => {
        setflag(!flag)
    }
   
    return (
        <div>
               {
                    menu === false ? null
                        : <Slider slide={showMenuBar} />
                }
            {
                    flag === false ? <div class="circle" onClick={showForm}>
                        <img src={Info} alt="info" />
                    </div> : <div className="form">
                            <div className="head">
                                <div></div>
                                <div>Admission Inquiry</div>
                                <div onClick={showForm} style={{ cursor: 'pointer' }}>X</div>
                            </div>
                            <div className="form-content">
                                <div className="icon-container">
                                    <span className="icon">
                                        <img src={Social} alt="social" />
                                    </span>
                                    <span className="text-content">
                                        Hello! Welcome to Flora college
                              </span>
                                </div>
                                <div className="icon-container1">
                                    <span className="icon">
                                        <img src={Social} alt="social" />
                                    </span>
                                    <span className="text-content" style={{ display: 'flex', flexDirection: 'column' }}>
                                        Please Tell a bit about yourself.
                              <div style={{ marginTop: 15, width: '70%' }}>
                                            <div>Your Name</div>
                                            <input onChange={(e)=>setname(e.target.value)} type="text" placeholder="full name" />
                                        </div>
                                        <div style={{ marginTop: 15, width: '70%' }}>
                                            <div>Your E-mail Address</div>
                                            <input onChange={(e)=>setemail(e.target.value)} type="text" placeholder="E-mail" />
                                        </div>
                                        <div style={{ marginTop: 15, width: '70%' }}>
                                            <div>Contact Number</div>
                                            <input onChange={(e)=>setcontact(e.target.value)} type="text" placeholder="Contact Number" />
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div onClick={sendEmail} className="submit">
                                SUBMIT
                   </div>
                        </div>
                }
            <Fix_navigation_bar />    
            <Navigationbar slide={showMenuBar} />
              <div className="Prospecting_navigation_bar_sub">
                <Link to="/barch" className="inactive">B.Arch</Link>
                <Link to="/prospectingstudent" className="active">Prospecting Students</Link>
            </div>

            <div className="Prospecting-container">
                <div className="Prospecting-title">
                    <strong>Prospecting Student</strong>
                    <div className="Prospecting-line"></div>
                </div>
                <div className="">
                 <span className="Prospecting-subtitle">WHY ARCHITECTURE?</span> 
                    <p>
                    1. Mustering the courage to take on a new field is challenging even when the fulfillment comes from the assurance that this is what you wanted to do, the genuine happiness emerges from the appreciation and the recognition that one receives for it.<br/><br/> 2. Architecture today is only desk tentatively expressive of the human spirit having been tempted from its mission by the love of mechanization. Great buildings that move the spirit have always been rare. In every case they are unique, poetic, products of the heart, of sensibility and with a freshness of view, which shows us the way and remind us of our mission to inspire. <br/><br/>3. As in India massive infrastructure are coming up very fast, Development will be on a continuous rise for coming years in urban, suburban and rural areas. To pursue this need there is huge demand of well trained skillful architects who can design and execute these infrastructure. Even in the upcoming electronic and computer age, architects have bright future ahead, because electronic gadgets can not be creative . Hence perceptive people, especially those with an ability to visualize the design for an updated reality are going to be in great demand in the new millennium.
                    </p>
                </div>

                <div className="">
                 <span className="Prospecting-subtitle">WHY FCOA?</span> 
                    <p>
                    1. Mustering the courage to take on a new field is challenging even when the fulfillment comes from the assurance that this is what you wanted to do, the genuine happiness emerges from the appreciation and the recognition that one receives for it.<br/><br/> 2. Architecture today is only desk tentatively expressive of the human spirit having been tempted from its mission by the love of mechanization. Great buildings that move the spirit have always been rare. In every case they are unique, poetic, products of the heart, of sensibility and with a freshness of view, which shows us the way and remind us of our mission to inspire. <br/><br/>3. As in India massive infrastructure are coming up very fast, Development will be on a continuous rise for coming years in urban, suburban and rural areas. To pursue this need there is huge demand of well trained skillful architects who can design and execute these infrastructure. Even in the upcoming electronic and computer age, architects have bright future ahead, because electronic gadgets can not be creative . Hence perceptive people, especially those with an ability to visualize the design for an updated reality are going to be in great demand in the new millennium.
                    </p>
                </div>
            </div>

            <Footer/>
        </div>
    )
}

export default ProspectingStudent
