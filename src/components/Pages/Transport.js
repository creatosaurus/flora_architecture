import React, { useState } from 'react'
import Navigationbar from '../ReusableComponents/Navigation_bar'
import Fix_navigation_bar from '../ReusableComponents/Fix_navigation_bar'
import Footer from '../ReusableComponents/Footer'
import { Link } from 'react-router-dom'
import Info from '../../assets/commerce-and-shopping.svg'
import Social from '../../assets/social.svg'
import emailjs from 'emailjs-com';
import Slider from '../ReusableComponents/Slider'

function Transport() {
    const [flag, setflag] = useState(false)
    const [name, setname] = useState("")
    const [email, setemail] = useState("")
    const [contact, setcontact] = useState("")
    const [menu, setMenu] = useState(false)

    const showMenuBar = () => {
        setMenu(!menu)
    }


    const sendEmail = () => {

        var template_params = {
            "name": name,
            "email": email,
            "contact": contact
        }
        emailjs.send('default_service', 'template_QcFeJJkU', template_params, 'user_pMyeJVJzDOA6CDIxE2aM8')
            .then((result) => {
                alert('Form Submitted')
            }, (error) => {
                alert('Please try again')
            });
    }


    const showForm = () => {
        setflag(!flag)
    }

    return (
        <div>
            {
                menu === false ? null
                    : <Slider slide={showMenuBar} />
            }
            {
                flag === false ? <div class="circle" onClick={showForm}>
                    <img src={Info} alt="info" />
                </div> : <div className="form">
                        <div className="head">
                            <div></div>
                            <div>Admission Inquiry</div>
                            <div onClick={showForm} style={{ cursor: 'pointer' }}>X</div>
                        </div>
                        <div className="form-content">
                            <div className="icon-container">
                                <span className="icon">
                                    <img src={Social} alt="social" />
                                </span>
                                <span className="text-content">
                                    Hello! Welcome to Flora college
                              </span>
                            </div>
                            <div className="icon-container1">
                                <span className="icon">
                                    <img src={Social} alt="social" />
                                </span>
                                <span className="text-content" style={{ display: 'flex', flexDirection: 'column' }}>
                                    Please Tell a bit about yourself.
                              <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Your Name</div>
                                        <input onChange={(e) => setname(e.target.value)} type="text" placeholder="full name" />
                                    </div>
                                    <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Your E-mail Address</div>
                                        <input onChange={(e) => setemail(e.target.value)} type="text" placeholder="E-mail" />
                                    </div>
                                    <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Contact Number</div>
                                        <input onChange={(e) => setcontact(e.target.value)} type="text" placeholder="Contact Number" />
                                    </div>
                                </span>
                            </div>
                        </div>
                        <div onClick={sendEmail} className="submit">
                            SUBMIT
                   </div>
                    </div>
            }
            <Fix_navigation_bar />
            <Navigationbar slide={showMenuBar} />
            <div class="transport-main-container">
                <div class="transport-sub-nav">
                    <Link to="/academicwork" class="inactive student-corner-sub-nav-item">Academic Work</Link>
                    <Link to="/ict" class="inactive student-corner-sub-nav-item">ICT</Link>
                    <Link to="/hostel" class="inactive student-corner-sub-nav-item">Hostel</Link>
                    <Link to="/library" class="inactive student-corner-sub-nav-item">Library</Link>
                    <Link to="/sport" class="inactive student-corner-sub-nav-item">Sports</Link>
                    <Link to="/transport" class="active student-corner-sub-nav-item">Transport</Link>
                    <Link to="/computerlab" class="inactive student-corner-sub-nav-item">Computer lab</Link>
                    <Link to="/materiallibrary" class="inactive student-corner-sub-nav-item">Material Library</Link>
                </div>
                <div className="transport-container">
                    <div className="transport-container-header">
                        <div>
                            TRANSPORT
                    </div>
                        <div className="transport-container-border">

                        </div>
                    </div>
                    <div></div>
                    <div class="transport-container-paragraph">
                        FCOA is located on the outskirts of Pune, near Khed Shivapur toll plaza, so we make sure that a convenient and easily accessible transport facility is made available for all the students as well as teaching and non- teaching staff. We have 2 buses running on 2 dedicated routes in Pune city. Both the routes have several stops where students can easily catch the bus. So, do not worry about the distance from Pune city and transport because we have got you covered there!
                </div>
                    <div class="transport-ICT-card-container">
                        <div class="transport-ICT-card">

                        </div>
                        <div class="transport-ICT-card">

                        </div>
                        <div class="transport-ICT-card">

                        </div>
                        <div class="transport-ICT-card">

                        </div>

                    </div>


                </div>

            </div>
            <Footer />
        </div>

    )
}

export default Transport
