import React, { useState, useEffect } from 'react'
import Navigationbar from '../ReusableComponents/Navigation_bar'
import Info from '../../assets/commerce-and-shopping.svg'
import LineTitle from '../ReusableComponents/Line_Title'
import Button from '../ReusableComponents/Button'
import Card from '../ReusableComponents/Card'
import Footer from '../ReusableComponents/Footer'
import Social from '../../assets/social.svg'
import { Link } from 'react-router-dom'
import emailjs from 'emailjs-com';
import Slider from '../ReusableComponents/Slider'


const img0 = require('../../assets/carousel1.JPG')
const img1 = require('../../assets/carousel 2.JPG')
const img2 = require('../../assets/carousel3.JPG')
const img3 = require('../../assets/carousel4.JPG')
const img4 = require('../../assets/A perfect place for creative minds to work.jpeg')
const img5 = require('../../assets/ajanta ellora.png')
const img6 = require('../../assets/Ample light & Ventilation.jpeg')
const img7 = require('../../assets/art exhibition 0.JPG')
const img8 = require('../../assets/art exhibition 1.JPG')
const img9 = require('../../assets/art exhibition 2.JPG')
const img10 = require('../../assets/art exhibition 3.JPG')
var i = 0;



const slideImages = [
    img0,
    img1,
    img2,
    img3,
    img4,
    img5,
    img6,
    img7,
    img8,
    img9, img10
];

const studentStories = [
    {
        img: require('../../assets/student.png'),
        paragraph: 'Proud to pursuing B.Arch in this institute. The nature around the college is really very mesmerizing',
        name: 'Rama Pawaskar'
    },
    {
        img: require('../../assets/student.png'),
        paragraph: 'One of the best architectural institute, a peaceful environment',
        name: 'Sumedh Gadankush'
    },
    {
        img: require('../../assets/student.png'),
        paragraph: 'good faculty and also good coordination between students and teachers. Pleasing environment',
        name: 'Saloni Shirke'
    },
    {
        img: require('../../assets/student.png'),
        paragraph: 'Good coordination between faculty member and students, full of passion and virtues to imbibe the best version of students.',
        name: 'Shivam Patil'
    }

]

const Home = ({ slide }) => {

    const [flag, setflag] = useState(false)
    const [index, setIndex] = useState(0)
    const [key, setKey] = useState(0)
    const [name, setname] = useState("")
    const [email, setemail] = useState("")
    const [contact, setcontact] = useState("")
    const [menu, setMenu] = useState(false)

    const showForm = () => {
        setflag(!flag)
    }

    const sendEmail = () => {

        var template_params = {
            "name": name,
            "email": email,
            "contact": contact
        }
        emailjs.send('default_service', 'template_QcFeJJkU', template_params, 'user_pMyeJVJzDOA6CDIxE2aM8')
            .then((result) => {
                alert('Form Submitted')
            }, (error) => {
                alert('Please try again')
            });
    }


    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])

    const backbutton = () => {
        if (i > 0) {
            i = i - 1
            setIndex(i)
            if (slideImages[i] == null) {
                i = 0;
                setIndex(i)
            }


        }
        else {
            i = 0;
            setIndex(i)
        }


    }
    const forwadbutton = () => {
        if (i < slideImages.length) {
            i = i + 1
            setIndex(i)
            if (slideImages[i + 2] == null) {
                i = 0;
                setIndex(i)
            }

        }
        else {
            i = 0;
            setIndex(i)
        }

    }

    const studentbackbutton = () => {
        if (i > 0) {
            i = i - 1
            setKey(i)
            if (studentStories[i] == null) {
                i = 0;
                setKey(i)
            }


        }
        else {
            i = 0;
            setKey(i)
        }


    }
    const studdentforwadbutton = () => {
        if (i < studentStories.length) {
            i = i + 1
            setKey(i)
            if (studentStories[i] == null) {
                i = 0;
                setKey(i)
            }

        }
        else {
            i = 0;
            setKey(i)
        }
    }
    const showMenuBar = () => {
        setMenu(!menu)
    }

    return (
        <div>
            {
                menu === false ? null
                    : <Slider slide={showMenuBar} />
            }
            <nav className="fix_navigation_bar">
                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                    <i class="material-icons" style={{ marginRight: 10, width: 25 }}>email</i>
                    <span>flora.architecture@flora.ac.in</span>
                    <i class="material-icons" style={{ marginLeft: 10, marginRight: 10, width: 25 }}>phone</i>
                    <span>9890-673-701</span>

                </div>
                <a href="#admission" onClick={showForm} style={{ marginLeft: '5%' }}>
                    Admission open 2020-2021
           </a>
                <div >
                    <Link to="/news" href="#study">News</Link>
                    <a data-target="events" href="#events">Events</a>
                    <Link to="/barch">Admission</Link>
                    <Link to="/contact" >Contact</Link>
                </div>
            </nav>
            <Navigationbar slide={showMenuBar} />
            <div className="image-back" style={{ backgroundImage: `url(${img0})` }} >
                <div className="block">
                    <div className="one">We are Changing the World</div>
                    <div className="second">& Our Weapon is Quality Education</div>
                </div>
                <div className="content-block">
                    <div className="title">
                        <div>Quick</div>
                        <div>Facts</div>
                    </div>
                    <div className="quick-fact-container">
                        <div className="number">1.1million</div>
                        <div className="sub-title">Subscription of<br /> e-books books</div>
                    </div>
                    <div className="quick-fact-container">
                        <div className="number">2 million</div>
                        <div className="sub-title">Subscription of<br /> e-journal articles</div>
                    </div>
                    <div className="quick-fact-container">
                        <div className="number">300+</div>
                        <div className="sub-title">Hostel capacity for students</div>
                    </div>

                    <div className="quick-fact-container">
                        <div className="number">17 Acre</div>
                        <div className="sub-title">Lush green campus</div>
                    </div>

                </div>

                {
                    flag === false ? <div class="circle" onClick={showForm}>
                        <img src={Info} alt="info" />
                    </div> : <div className="form">
                            <div className="head">
                                <div></div>
                                <div>Admission Inquiry</div>
                                <div onClick={showForm} style={{ cursor: 'pointer' }}>X</div>
                            </div>
                            <div className="form-content">
                                <div className="icon-container">
                                    <span className="icon">
                                        <img src={Social} alt="social" />
                                    </span>
                                    <span className="text-content">
                                        Hello! Welcome to Flora college
                              </span>
                                </div>
                                <div className="icon-container1">
                                    <span className="icon">
                                        <img src={Social} alt="social" />
                                    </span>
                                    <span className="text-content" style={{ display: 'flex', flexDirection: 'column' }}>
                                        Please Tell a bit about yourself.
                              <div style={{ marginTop: 15, width: '70%' }}>
                                            <div>Your Name</div>
                                            <input onChange={(e) => setname(e.target.value)} type="text" placeholder="full name" />
                                        </div>
                                        <div style={{ marginTop: 15, width: '70%' }}>
                                            <div>Your E-mail Address</div>
                                            <input onChange={(e) => setemail(e.target.value)} type="text" placeholder="E-mail" />
                                        </div>
                                        <div style={{ marginTop: 15, width: '70%' }}>
                                            <div>Contact Number</div>
                                            <input onChange={(e) => setcontact(e.target.value)} type="text" placeholder="Contact Number" />
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div onClick={sendEmail} className="submit">
                                SUBMIT
                   </div>
                        </div>
                }




            </div>

            <div className="vision" style={{ marginTop: 200 }}>
                <div className="grid">
                    <LineTitle title="FLORA COLLEGE OF ARCHITECTURE" />
                    <p class="left">Flora College of Architecture is a place where brilliant minds assemble and collaborate, where they pool together their individual talents across disciplines in service of big projects and big ideas. It is a vibrant community teaming with students
                    collaborating with experts and specialists: a place of innovation and creativity. It is an intersection of disciplines, a launching pad for a brilliant career, and a highly unique state of mind. It is a perfectenvironment to pursue your passion.
                     Here, the future is envisioned each day.</p>
                </div>
                <div className="grid2">
                    <img src={require('../../assets/carousel3.JPG')} alt="student" />
                </div>
            </div>

            <div className="brech" style={{ marginTop: 100, marginLeft: '5%' }}>
                <LineTitle title="COURSES OFFERED" styles={{ width: '5%' }} />
                <div className="title left1">
                    <div>
                        Bachelor of Architeture
                    </div>
                </div>
                <p className="left1">
                    The College offers an undergraduate degree in Architecture (intake – 40 seats). The admission
                    processes are governed by the guidelines of Council of Architecture, New Delhi (COA) and the
                    Savitribai Phule Pune University.
                </p>
                <div className="left1">
                    <Button to="/barch" title="Learn More" />
                </div>
            </div>

            <div className="quote">
                <div className="quote-title">FLORA COLLEGE OF ARCHITECTURE</div>
                <div className="quote-subtitle">Architecture is an art we live in</div>
            </div>


            <div className="news-events">
                <div id="events">
                    <LineTitle title="EVENTS" />
                    <div>
                        <Card eventname="Governing Council Visit" venue="Flora Institute" />
                        <Card eventname="Cleanliness Drive" venue="Flora Institute" />
                        <Card eventname="Excellentia" venue="Flora Institute" />
                        <Card eventname="Annual Exhibition" venue="Yashwantrao Chavan Art Gallery " />
                        <div className="left2">
                            <Button to="/events" title="More Events" />

                        </div>
                    </div>
                </div>
                <div id="study">
                    <LineTitle title="Study Tours & Case Studies" />
                    <div style={{ display: 'flex', marginLeft: '12%', marginTop: 15 }}>
                        <img className="card3-box" src={require('../../assets/jala srushti 1.JPG')} />
                        <div className="box-content">
                            <p>Jala Srushti</p>
                            <span>21 Mar,2020</span>
                        </div>
                    </div>
                    <div style={{ display: 'flex', marginLeft: '12%', marginTop: 15 }}>
                        <img className="card3-box" src={require('../../assets/harappa 1.jpg')} />
                        <div className="box-content">
                            <p>Harappan Civilisation</p>
                            <span>21 Mar,2020</span>
                        </div>
                    </div>

                    <div style={{ display: 'flex', marginLeft: '12%', marginTop: 15 }}>
                        <img className="card3-box" src={require('../../assets/north karnataka 1.png')} />
                        <div className="box-content">
                            <p>North Karnataka</p>
                            <span>21 Mar,2020</span>
                        </div>
                    </div>

                    <div style={{ display: 'flex', marginLeft: '12%', marginTop: 15 }}>
                        <img className="card3-box" src={require('../../assets/ajanta ellora.png')} />
                        <div className="box-content">
                            <p>Ajanta-Ellora</p>
                            <span>21 Mar,2020</span>
                        </div>
                    </div>

                    <div className="left2">
                        <Button to="/studycase" title="More Tours" />
                    </div>
                </div>
            </div>

            <div class="placement">
                <LineTitle title="Gallery" styles={{ width: '5%' }} />
                <i onClick={backbutton} class="material-icons back-button" style={{ marginRight: 10, fontSize: 50 }}>navigate_before</i>
                <div class="grid">
                    <div class="gallery-img-container">
                        <img class="home-image" src={slideImages[index]} alt="" />
                    </div>
                    <div class="gallery-img-container">
                        <img class="home-image" src={slideImages[index + 1]} alt="" />
                    </div>
                    <div class="gallery-img-container">
                        <img class="home-image" src={slideImages[index + 2]} alt="" />
                    </div>
                </div>
                <i onClick={forwadbutton} class="material-icons next-button" style={{ marginLeft: 10, fontSize: 50 }}>navigate_next</i>
            </div>


            <div className="student-main-container">
                <div className="student-header">
                    <LineTitle title="OUR STUDENT STORIES" styles={{ width: '5%' }} />
                </div>
                <div className="student-stories">
                    <i onClick={studentbackbutton} className="material-icons student-back-button">navigate_before</i>
                    <div>
                        <div className="student-content">
                            <p>{studentStories[key].paragraph}</p>
                            <strong>{studentStories[key].name}</strong>
                        </div>
                    </div>
                    <i onClick={studdentforwadbutton} className="material-icons student-next-button">navigate_next</i></div>
            </div>
            <Footer />
        </div>


    )
}

export default Home
