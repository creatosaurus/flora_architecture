import React, { useState } from 'react'
import Navigationbar from '../ReusableComponents/Navigation_bar'
import Footer from '../ReusableComponents/Footer'
/* using the notefromdirector css */
import Info from '../../assets/commerce-and-shopping.svg'
import Social from '../../assets/social.svg'
import { Link } from 'react-router-dom'
import emailjs from 'emailjs-com';
import Slider from '../ReusableComponents/Slider'
import Fix_navigation_bar from '../ReusableComponents/Fix_navigation_bar'

const Principlemessage = () => {
    const [flag, setflag] = useState(false)
    const [name, setname] = useState("")
    const [email, setemail] = useState("")
    const [contact, setcontact] = useState("")
    const [menu, setmenu] = useState(false)

    const showMenuBar = () => {
        setmenu(!menu)
    }

    const sendEmail = () => {

        var template_params = {
            "name": name,
            "email": email,
            "contact": contact
        }
        emailjs.send('default_service', 'template_QcFeJJkU', template_params, 'user_pMyeJVJzDOA6CDIxE2aM8')
            .then((result) => {
                alert('Form Submitted')
            }, (error) => {
                alert('Please try again')
            });
    }


    const showForm = () => {
        setflag(!flag)
    }

    return (
        <div>
             {
                menu === false ? null
                    : <Slider slide={showMenuBar} />
            }
            {
                flag === false ? <div class="circle" onClick={showForm}>
                    <img src={Info} alt="info" />
                </div> : <div className="form">
                        <div className="head">
                            <div></div>
                            <div>Admission Inquiry</div>
                            <div onClick={showForm} style={{ cursor: 'pointer' }}>X</div>
                        </div>
                        <div className="form-content">
                            <div className="icon-container">
                                <span className="icon">
                                    <img src={Social} alt="social" />
                                </span>
                                <span className="text-content">
                                    Hello! Welcome to Flora college
                              </span>
                            </div>
                            <div className="icon-container1">
                                <span className="icon">
                                    <img src={Social} alt="social" />
                                </span>
                                <span className="text-content" style={{ display: 'flex', flexDirection: 'column' }}>
                                    Please Tell a bit about yourself.
                              <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Your Name</div>
                                        <input onChange={(e) => setname(e.target.value)} type="text" placeholder="full name" />
                                    </div>
                                    <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Your E-mail Address</div>
                                        <input onChange={(e) => setemail(e.target.value)} type="text" placeholder="E-mail" />
                                    </div>
                                    <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Contact Number</div>
                                        <input onChange={(e) => setcontact(e.target.value)} type="text" placeholder="Contact Number" />
                                    </div>
                                </span>
                            </div>
                        </div>
                        <div onClick={sendEmail} className="submit">
                            SUBMIT
                   </div>
                    </div>
            }

            <Fix_navigation_bar />
            <Navigationbar slide={showMenuBar} />
            <div className="about-sub-nav">
                <Link to="/about" className="inactive about-sub-nav-item">About us</Link>
                <Link to="/vision" className="inactive about-sub-nav-item">Vision</Link>
                <Link to="/mission" className="inactive about-sub-nav-item">Mission</Link>
                <Link to="/notefromdirector" className="inactive about-sub-nav-item">About Director</Link>
                <Link to="/principlemessage" className="active about-sub-nav-item">Principal's Message</Link>
                <Link to="/governingcounsil" className="inactive about-sub-nav-item">Governing Council</Link>
            </div>
            <div className="p-note-from-director-conteiner">
                <div className="p-note-from-director-title">
                    <strong>PRINCIPAL's MESSAGE</strong>
                    <div className="p-about-line"></div>
                </div>
                <div className="p-note-from-director-grid">
                        <div class="p-note-from-director-image"><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wEEEAANAA0ADQANAA4ADQAOABAAEAAOABQAFgATABYAFAAeABsAGQAZABsAHgAtACAAIgAgACIAIAAtAEQAKgAyACoAKgAyACoARAA8AEkAOwA3ADsASQA8AGwAVQBLAEsAVQBsAH0AaQBjAGkAfQCXAIcAhwCXAL4AtQC+APkA+QFOEQANAA0ADQANAA4ADQAOABAAEAAOABQAFgATABYAFAAeABsAGQAZABsAHgAtACAAIgAgACIAIAAtAEQAKgAyACoAKgAyACoARAA8AEkAOwA3ADsASQA8AGwAVQBLAEsAVQBsAH0AaQBjAGkAfQCXAIcAhwCXAL4AtQC+APkA+QFO/8IAEQgBOAE4AwEiAAIRAQMRAf/EABsAAAIDAQEBAAAAAAAAAAAAAAABAgMEBQYH/9oACAEBAAAAAPo4IAAAAAEwAAAABiAAABiAEwAAAABgIAAACOSzSAAAAMTTExAABzvOeO5dD6/a9T3rhgDTE0NAAAHP+Z+eqgVRm9Gr33s7QAGIaaAAAzfH+NBQSSHa/U/U9QAANDQAAHyjyUFGQ5RUQl6v6jrYAAAAAByvitBK+++0VePJGG36t6VjYAAAAB4D5yaen15W3Qhnxc7Lnj9T9ReMYwGIACr5R5jT3ejqHJU001ZsHNn9c7rGwYMAAKvhse/ptmrNDy5oxWPh5+v9lm2NgSBoBV/D+h2RucNE8+bOnRy8NX2zoykNjJiAEl8Th6d7ZhGSqpx5487kx+09GbkxsmIBAvk+ftirt2ks2aEc8OVx5fYOvZKTbZMEJAeF5cc8sVOvs5uUXRp1PzD+x7rLJSYOxAk0LhfOe1BQ2cnRRXvv5Snjhz/sGi2yc2DsQKLCPifJZtXRiQ3Z+bueTmzl0bvfXWWTsYFqAixR8x5mnqxU8m6jkbSpYLDT766dk5yY7EAhIy+KhdKOHXoo5dPTdeSFHd9XZKyycmFgAhJQ8ty73yWdjJw6tHZz5o1+w6k5WTnJssAEJKHleJZjpu4cKMm7Z1nS+n6q2cp2Tk2WiASSXlfP18DOZ4EZWae7XZ6js2uU52SbLGgQko8XyUPKx3bSmqnnx9RHR67oTk5znNssABJKNPgc/P0w0dCPI56Nx1PW6JTc5zcmrQBCRFeR85TdpFVgqgPd6TuWzc7JykMsAEgilzvC4pToz1Fiqt7frr5zlKc5SYWiBAJRo8Zxy6wjRmy17u/6S2cnOVkmwsAQCIZocryue/TZDBzqbL93t7rJOU5SkwsAQCz8aXSq8JRlytZS4q9d6TVY5uc5NhOl2goUZ+D37Ycr541XGMS2X0HTffNytcpAYaNE9iyUtcrrWFPkvO2U1Rs0YPX96cp6LJucmw5QWVwJK3n9C2XD4/IOjXjhlv8AfxucJabrJOQHCthdypbXJY+hoj43rX/PtkrTH7DTo0RKNF2iyTYcWpbOSuhbVno6+TknYs8ry908XZu8r2PVXRhKb1XuQcfLffzodB4pcrNo7+bacXy3Rl0qYz59u7raa7WrtUw5mPbLJDTkp28Dq26cm2a8F0OpUr7IajmbN85qL03vkZ+kq6c9Gp4t6uybpPluUb7KbbiknOxKLt0cQ6cM+eo6GKGid2DdZTROcpmXRa6nOFkWjNp5T6tWSC06uUXap8/ZZSXqRCjRY4MBTdeaz//EABgBAQEBAQEAAAAAAAAAAAAAAAABAgME/9oACAECEAAAAAAAAAWAABQQABMy60gAOfKGunUgKk4ZsW99CWKmeOQ130EFmMac91rcqAc5ljq01VgGOCrG+tKgY4dNaYxvqUSmeHQrn13KIU4xqW7oIUcIu7sANbnDDU6bzKOjGtGfJm79NzkOoDj5Xq7IxDqFM+P0dQYy6gLACY6AAEK//8QAGAEBAQEBAQAAAAAAAAAAAAAAAAEDAgT/2gAIAQMQAAAAgAAAAAAAAAAL1pZnzAAXb0U5wxgAdevorjy8ABp6bSTzZgBrpOtMubxkAHpdu82c44AGvqpzZlhyANPXnzxNNMcAA69edTnTDMAHo7qc84SgB3vbM+cgAG/aMswAHXp7nPlgBYLp6Xnz5CUsC+rLOEIoBYASgAAP/8QAOxAAAgIBAgMFBQYFAwUBAAAAAQIAAxEEIQUSMRATMkFRFCJAYXEGIEJSgZEVIzBysSRToTNQYGKC0f/aAAgBAQABPwD/AMfZlQZYhR84/EuHVnD6ylT6FxKdZpNRtTqarP7WB/7FqOLcN0ue+1dan0Bz/ia37YaZVZdHUzt+d9lmp4/xW8nm1Tgei7CPq7X8drH6sYbF9IrlWBRiD8pVx/itICjVuRNJ9r9bWR3ipak0v2q4VfgOzUt/7Sm+m9eeq1HX1Ug/GcS4lp+G6c3XZP5VHVjOJfaTXa44B7ur8gjXGF3M5sQsp84SZzQWQWFTBYGml1uo0dofT3FG8x5GcL+1tVgRdYoQ+oORKra7q1srcMjDIYfE6zV0aPT2X3NhEE4txOziOqe9z7nRE9AI1jGd4PlCYWhwZ9ZjtBInMDA5B6zgPH7OHuK3bNDHeabV0alA9Tgg/Efafij63WNQj/yKW/do1rbqJzP6Q79RMGBZywIYKp3ZENc5TnsPSK04JxW2kCjmwmcqfSaPUjUJnzHw3GdYNHw7UW82G5SF+pl1hJPqT1g5R0m5grY+UXTt6GLpWPlBo/lPYtuk9kYGexnPyjaM+Ql1BSMhxMbdmjteq2pk8QIM4PazsxTw8oJx03mfhftleXt0ulX0LmWeJl9DErLSujmM0+j/AFiaXYbT2cekXTrO5QQ1r6QosdZdQrS3S+kNBzGQCVFVtU2Z5cjOJwSzRW0r7O+Mbkc2/wCsqfOQeszMzPwLlsjE+0VxbimocncbQADLN1lFDWkGU6VUAzKwggIxD+kzBkxgRGjCcmY9SxqQ3lNRQBnAjKCD6+U0urv0dyXVPyss4PxSniVItQjnwA6+h+EswqlvQGay5tRqbXbcliZVp2sBI3M0+nFaKMdBAu8XCzvRic7RHXz6xWEYiPCwE7yE5mJcAcy4crmWDf5ETgPEn4frUbP8tyFcfImKwImfgrF50YeoM19SUa3U0p0W1gPoJoQnd++eUf8AJlYXk27AmZ3JncNjYzkZTvFyBMx0PrGp+cNTCbiZlkuXqYRkfTMWcLtNvD9G7dTQmYPgj0nGa2r4rrQ3XvmM0wZyBvFXlRRFRjEp9YVAg+UZQYBvCkKriFBiGrMsqO57HG01LDlPrObxQDcThw5NFpV9Kk/xAYPgvtLTjil59d5wukkknwr2C5Vh4jWvnP4khlerRyAIGUgb9gYYhuXml2sVGxkYg11J6HeHUIYxUnbs1lRXLDpPNoJwlnOg03N/tL/iAwH4L7Uab36nRSXacPqNens5wFPOfEwEuvQZUWJn+4StA3V1H6xtNpyu7p+8sor37tx+jRVvqPMqt+0r1znC8sr1LcmyEiX6of2mPrTk4EFVt5BKkfpP4ePxWY/4j6Vx4LAf/pYBeh3RiPpM7CMi2ow+UuQq5HoZUMso+YmjQV6elB0CARTAYPgePVo3D73I3CRWT2kF1DIoHunoYOKqqKiU1qB6KI3HXzVyqvutnMHGAtrv3LMW/baV8VqubuO4C59RzSwaLkR2RFbGdsq0flLEofM+L3pptcysUahWGPwFtpqdWruE9m6nzaVMqZLKFGTsvu/87maa7SWuR7NzYH4mLEy7VaPRoAdOhYkg+7PbKHsY9wvLgQ36ViMUrOUHBrZ1/taHU6muwoXDe8BkqPOcW0BpfvkbKOASPQmaRGe+oKMnmEq8C/QQQQQf0wRk/d+1Grem2qvkyj1kGWVKWZ0LY5R1nIW6kyvT1jG0WutR/wBQCd3Wu/OCfpAruchMzUq9VeVTGR1mgoWrTrnxHcn6zX1F0bHUD/EpJZyG3+ZiqE/D+sKBuqqfrLaFwcbQ0shJDQPcPxxVay0f3DP6TiesFmNOunOSo98tOA6ANqOezGV3Ai+UEEEH9Pz+79ptOLNNVZjdHhr59JXjycj95VoxzAmLQmBmHTU/ki01L+AQkAYAltD6nCF+VMgnHygUKoUdAJauRGqCOcSshlAMagHzInsv/tDpkHlLqCu4mmQZziahc6rI6hBODG7+Ipkkgg5i+UEEEEH9fVUrqKLKm6Mphp7ipkP5zkRYGx853iGNfSvWHVG2zu6V39T5RK2Qbtlid4ACJcCBLauduuJ350zqtoyp6MIltNiggz3fWOyyzEpGGMs21jfMCcGo/wBQ9mNlXEXtEEH9c9ZxgKl+3mATFK4BisvpGZQDsJe3OcAbylDpFZ8ZJ6yri+mclTlT8xKtUjJtgiazV18m+IeIUL1ePqE1WAvSaN1VipE9wgbCWKu5hEUYMtXOoHrgTh9Pc0KD4juf1i9gg+AM85xzbUD+2c2wgs2mo1BUHJlF1fjLby3WJjyP6y/UJnZBmVay7OEOJqLbWJ53zObeUXBek75Mhs4Mp1AK7nMssJG055WxLTT6XvtTUcbKAT+kQQQdgg/rmeU48uLaj6jsPNiazUFnx6RHb1MsdiB1gVj6wl18jMu3kYVPpFR9jMkTS6k1uMn3TDGlPjE4dX4m+giwdogg/rmcap59Or/lM2Bj+E46y6p+djE56/e5CwlVT6oEpVsPWJoNSgU+yls9Mbw121bvo7B9UlyXPt7LYP8A5nsmqHL/ACMZ6Zl6207PXGLNvy7RPGog8CiGUKczSJihBB9wfBX1C6p0PmDNRW1bsp6gzO0dPeO0qpTlII6id09LEIxAlOp1lTowclV/Cek/iFtinmFY/Q+Uv4lc3RE/Yy/V6h1AZunmBiOLLmy7kxqxjlEppQOGI3jPE3mmrLOi+pEQAACD7gg7B8Bxmgpcz+TAGMYd5W09x+sUvXnBB+s71s+BY9jt0wP0jgZPMYWXosHWcxBnNmUrmcMo3Np+ggg7RB8HxKjvdOW/Em81FRUkjt3Ijm9ehjXXDrO8ubzgrsbqZyBRMQ7mVqSZo9ObbFQdPOV1hFCjoBF7PLsEEHwVx2K/vNbT3bun7frHTc4iekVJ3HNPYU8942jRfKNUqx1xHgBMpWcKuUWtX5lYIPuCD4FmVRknE9rqL8i5JhyTOJafvK+dfEscTdTEtUdYlyzvwPONcuDLr0Bll2TMkxBO8CCcFY26y5/JQBKzkQQdo+BsuCg4ms1RVGdjsJw5GNXfOPefy9B2OAQROIZ0Oq5HH8pyeVvSHlYSznXpvDqyp6GDX/KPrSRtmG52g52igCGzyWWOVX5zgWlNNAZurbmKSDFI7R2D75IHWHUIOm8W5D12+s69PuM6r1Mawt9JZvkTWKXtqqPRmGf0lWyKOxpxPQ16zTsjD6GFL9K71n8J3E9pPmIba3hWuYScyCGyAO85Agmg051erVcZRdzKa+RYJW3ke3EBg++7sx3M5iG3gwREs7th+UnsJABJjXFumwh6zIHZev8AqaT6ExOg7D0hxg56TiulTVU+16YhinX5gSrurgCPdMOiVvL9Y2gI6WQ6Nv8AcE9lx+PMTSj0/eciqJqH8hODcPOnoHMP5j7tCOUAQTEFpU4b94rAjsExB95X5tjG3HzlT+Rlp90z2p8bvDqC/U5nPOf1nNAZqtrKz85WdoOzjdxp4fdjq+FE+z9FoDKXPIPwmcW0DcP1RdBilztKbiMbxXRx7yiWCryQQ48oTLHY7CcL0LanVBm8CEEypAqx/HMdlg2z6RD6RXgMH38kNMgj5xmxho5Pdxycys7xekxBgdZ3m+01bZsqEq8IgjMqgljgATimqOuuUJnuqzt8yZw5TSNxNZSusquqYe6RsT6iBWqdkOVZWIP1Er1LKMHeG136SvTs2C2ZbWijAAzGX0nDNGNNp0U+M7tNVxPSaV6q7HGWPl5fMzHvZzkes6jtXKOV/aA+sV8RWB+/au8UnOJ4nCepluOXEsG5idZX0mI0z70c8+pX5CJ0E1HEtPS3Jnns/Iu5lvt2tf8AmHkq/Isp0q9MSpMIoPltO79JxnRMlp1KgBWwGHzle/UCVj5RthLMsfnNHw9dOov1QwfwrNTxF+QrQOUfmPWWUtZYSSWJO5M0HFjpMUagM1Q6EdVlOoovXmptVh8j2uucN5iL1PYGIiWZ2gP3dT4SYBmvmmmUFuaWyzaIN4g2jsAI9hJiqSpMOrq01zlzlvJF3aajVcU1gK1juKz6H3pwzQnTtzvufMmYxKB7zTAmCRscETjWjbUacWjx1f4lJO0QxK2vtVAcf/glNOl0/vgM1g6FhLbmsbLHJhrDDpK9Km55YdLzu0r0LoeZGZW9QZXrdZQcWDvF/Zpp9ZRqPCcN+VuvYNiR2YnSJcR1gYH7jEW1SnwskpUKsaW15ldcyFEtszFBZocJUfpKqFax3xuWJldYAmMGfhlXjPYIw/Yjea6j2PWWVfh8S/QzR6RrVFj5VPL1aKErGK0C/wCYys53iUDOZ3XyjVlUi0qfKJSo8o9CMOks0hVwV2lOpuTC2pzD8wjYOGBg7MQiKxES3P1mewNyP8iZ4LyPWKMKBDGhbEdyYcmUpNW2KiPWUrgzG0MXwRdrTB0i9ICCCJrNJRdfTZYuXrBwPX6zlJnJgTkJi1AQIol3kIijEAnLGUZnIMjaFB6Renae1LcbNA2Zzh1PrFBtupb0zn9OwmO0JhiLkytcCas5ZViCZ7B0jbWxekXrGJViZg5gWFNoqgTEPSNu8UQCYmN5jcTE3VvuYnlGYkymzGAY+VORNK+bz9JmOYx37ak7NSf5sQwdYoglvjlZ90djHLExAScwCEe9MdjdIviMWDs855xescQdJ59mI+ymIJjef//EACMRAQACAQQCAwADAAAAAAAAAAEAEQIQICEwEjEDIkEyQGD/2gAIAQIBAT8A/wAGoRzJ5kMyCPflnUVZWlQUhncHsY+9tQmPrsXiMp2UkJg8dj7qUymOErmGE8WOMwK7FrKOdTynmrF+1zzYZwzGXUGzrzOL1qVAYwZfECjrz/joYcQwCURBjhxKmJ2JZP2eXEu9Ll8aYHHbkUyoNTnUIHYDM8bx0ZcBlVMfcd4XK0qBKAlTPGuZc4lxZgfsTfUSVA0/NEE5mQjPJlswLYFaJK2kdj6lROKnyY/WyMJ8WFFuxLibCfu9LKjhk5UTD4wOfcDSpWqROujZcvYlzxYf0jT/xAAjEQEAAwACAgICAwEAAAAAAAABAAIRITADEhAxIkATIEFg/9oACAEDAQE/AP8AgshVYeJn8THxMapM7SUpvLAD6mzZsQZamfUTsJX6/sy/DHroawmnyuTRjLiPZT62exPYhfmLxG89iFpd07APQyFNhSegED8chQjQjRJmsvXHOvxvOQIEZs02ESZjLu23r8b+UHiPkxwjdfqNrZCyQ8nOMXiXc7Bx2DpPXmZkxYjPXmLPI69tLbXJvxpNixeIuvbRxjBmRT4frvpbTJk5mQJd/wA7xx2VRNmEwl3CLr+h43HIMZ5LbwRP0Bx2e567LXX6izZsz5zs17z5e3//2Q=="
                class="p-note-img" /></div>
                    <div className="p-note-from-director-content">
                        <div className="p-note-from-director-sub-title">Ar. Rahuldev Patil</div>
                        <p>
                            The Flora Education Society's, College of Architecture, combines teaching and service to proactively develop the discipline of creative and innovative problem-solving to address the needs of our society.<br /><br />Graduates of the College of Architecture will participate in the contemporary environment, encourage, anticipate and respond to changes in the local, national and international communities.
The College of Architecture, with a program in Architecture, is dedicated to accomplishing its mission through excellence in teaching, research and service by preparing graduates for leadership roles in rebuilding cities and improving the quality of the built environment. <br /><br />FCOA is dedicated to educating future generations of ethical professionals, creative designers and informed citizens.
By offering an intense curriculum, led by an accomplished faculty, in a comprehensive studio and classroom environment, the College of Architecture program will educate students for significant roles as practitioners, developers and leaders in architecture, construction, community planning and community development. <br /><br />
Students will be challenged to develop their abilities in problem-solving, creative thinking and informed decision making, as a focus of their professional education. They will accomplish this in a nurturing and student-centred environment that fosters personal development and professional excellence.
                        </p>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default Principlemessage