import React from 'react'
import Button from '../ReusableComponents/Button'
import Fix_navigation_bar from '../ReusableComponents/Fix_navigation_bar'
import Navigationbar from '../ReusableComponents/Navigation_bar'
import Footer from '../ReusableComponents/Footer'

function News() {
    return (
        <div>
            <Fix_navigation_bar />
        <Navigationbar/>
        <div className="News-container" style={{marginTop:60}}>
        <div className="News-container-header">
            <div>
            News
            </div>
            <div className="News-container-border">

            </div>
        </div>
        <div className="News-card-header">
        
        <strong style={{marginTop:10}}>   1. Governing Council Visit
             </strong>
             <p style={{marginTop:10}}>
             Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when anunknown printer took a galley of type and scrambled it to make a type specimen book.
                
             </p>
        <strong style={{marginTop:10}} >Date</strong>
        </div>
        <div class="News-ICT-card-container">
                <div class="News-ICT-card">
                    <div className="News-image"></div>
                </div>
                <div class="News-ICT-card">
                <div className="News-image"></div>
               
                </div>
                <div class="News-ICT-card">
                <div className="News-image"></div>
               
                </div>
                <div class="News-ICT-card">
                <div className="News-image"></div>
               
                </div>
                
                <div className="news-center">
                    <Button to="/barch" title="Read More" />
                </div>
            

        </div>
        <div className="News-card-header">
        
        <strong style={{marginTop:10}}>  2. Cleanliness Drive</strong>
        <p style={{marginTop:10}}>
             Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when anunknown printer took a galley of type and scrambled it to make a type specimen book.
                
             </p>
        
        <strong style={{marginTop:10}}>Date</strong>
        </div>
        <div class="News-ICT-card-container">
                <div class="News-ICT-card">
                <div className="News-image"></div>
               
                </div>
                <div class="News-ICT-card">
                <div className="News-image"></div>
               
                </div>
                <div class="News-ICT-card">
                <div className="News-image"></div>
               
                </div>
                <div class="News-ICT-card">
                <div className="News-image"></div>
               
                </div>
                <div className="news-center">
                    <Button to="/barch" title="Read More" />
                </div>
            

        </div>
    </div>
    <Footer/>
    </div>
        
        )
}

export default News
