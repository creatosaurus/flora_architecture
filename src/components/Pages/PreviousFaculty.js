import React,{useState} from 'react'
import Navigationbar from '../ReusableComponents/Navigation_bar'
import Footer from '../ReusableComponents/Footer'
import { Link } from 'react-router-dom'
import Info from '../../assets/commerce-and-shopping.svg'
import Social from '../../assets/social.svg'
import emailjs from 'emailjs-com';

function PreviousFaculty() {
    const [flag, setflag] = useState(false)
    const [name, setname] = useState("")
    const [email, setemail] = useState("")
    const [contact, setcontact] = useState("")

  
    const sendEmail = () => {
        
        var template_params = {
            "name": name,
            "email": email,
            "contact": contact
        }
        emailjs.send('default_service', 'template_QcFeJJkU', template_params, 'user_pMyeJVJzDOA6CDIxE2aM8')
            .then((result) => {
                alert('Form Submitted')
            }, (error) => {
                alert('Please try again')
            });
    }
  

    const showForm = () => {
        setflag(!flag)
    }

    return (
        <div>
             {
                    flag === false ? <div class="circle" onClick={showForm}>
                        <img src={Info} alt="info" />
                    </div> : <div className="form">
                            <div className="head">
                                <div></div>
                                <div>Admission Inquiry</div>
                                <div onClick={showForm} style={{ cursor: 'pointer' }}>X</div>
                            </div>
                            <div className="form-content">
                                <div className="icon-container">
                                    <span className="icon">
                                        <img src={Social} alt="social" />
                                    </span>
                                    <span className="text-content">
                                        Hello! Welcome to Flora college
                              </span>
                                </div>
                                <div className="icon-container1">
                                    <span className="icon">
                                        <img src={Social} alt="social" />
                                    </span>
                                    <span className="text-content" style={{ display: 'flex', flexDirection: 'column' }}>
                                        Please Tell a bit about yourself.
                              <div style={{ marginTop: 15, width: '70%' }}>
                                            <div>Your Name</div>
                                            <input onChange={(e)=>setname(e.target.value)} type="text" placeholder="full name" />
                                        </div>
                                        <div style={{ marginTop: 15, width: '70%' }}>
                                            <div>Your E-mail Address</div>
                                            <input onChange={(e)=>setemail(e.target.value)} type="text" placeholder="E-mail" />
                                        </div>
                                        <div style={{ marginTop: 15, width: '70%' }}>
                                            <div>Contact Number</div>
                                            <input onChange={(e)=>setcontact(e.target.value)} type="text" placeholder="Contact Number" />
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div onClick={sendEmail} className="submit">
                                SUBMIT
                   </div>

                        </div>
                }
                             <Navigationbar />
            <div className="staff-sub-nav">
                <Link to="/faculty" className="inactive staff-sub-nav-item" >Faculty</Link>
                <Link to="/adminstaff" className="inactive staff-sub-nav-item" >Admin Staff</Link>
                <Link to="/previousfaculty" className="active staff-sub-nav-item">Previous Faculty</Link>
                <Link to="/staffactivity" className="inactive staff-sub-nav-item" >Staff Activity</Link>

            </div>

            <div className="faculty-principle-grid">
                <div className="principle">
                    <div className="principle-title">
                    </div>
                   
                </div>
                <div className="vice-principle">
                    <div className="vice-principle-title">
                    </div>
                    <div className="image-container">
                       
                    </div>
                </div>
            </div>
            <Footer/>


            
        </div>
    )
}

export default PreviousFaculty
