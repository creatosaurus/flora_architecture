import React, { useState } from 'react'
import Navigationbar from '../ReusableComponents/Navigation_bar'
import Footer from '../ReusableComponents/Footer'
import Info from '../../assets/commerce-and-shopping.svg'
import Social from '../../assets/social.svg'
import { Link } from 'react-router-dom'
import emailjs from 'emailjs-com';
import Slider from '../ReusableComponents/Slider'
import Fix_navigation_bar from '../ReusableComponents/Fix_navigation_bar'

const Mission = () => {
    const [flag, setflag] = useState(false)
    const [name, setname] = useState("")
    const [email, setemail] = useState("")
    const [contact, setcontact] = useState("")
    const [menu, setmenu] = useState(false)

    const showMenuBar = () => {
        setmenu(!menu)
    }

    const sendEmail = () => {

        var template_params = {
            "name": name,
            "email": email,
            "contact": contact
        }
        emailjs.send('default_service', 'template_QcFeJJkU', template_params, 'user_pMyeJVJzDOA6CDIxE2aM8')
            .then((result) => {
                alert('Form Submitted')
            }, (error) => {
                alert('Please try again')
            });
    }

    const showForm = () => {
        setflag(!flag)
    }

    return (
        <div>
            {
                menu === false ? null
                    : <Slider slide={showMenuBar} />
            }
            {
                flag === false ? <div class="circle" onClick={showForm}>
                    <img src={Info} alt="info" />
                </div> : <div className="form">
                        <div className="head">
                            <div></div>
                            <div>Admission Inquiry</div>
                            <div onClick={showForm} style={{ cursor: 'pointer' }}>X</div>
                        </div>
                        <div className="form-content">
                            <div className="icon-container">
                                <span className="icon">
                                    <img src={Social} alt="social" />
                                </span>
                                <span className="text-content">
                                    Hello! Welcome to Flora college
                              </span>
                            </div>
                            <div className="icon-container1">
                                <span className="icon">
                                    <img src={Social} alt="social" />
                                </span>
                                <span className="text-content" style={{ display: 'flex', flexDirection: 'column' }}>
                                    Please Tell a bit about yourself.
                              <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Your Name</div>
                                        <input onChange={(e) => setname(e.target.value)} type="text" placeholder="full name" />
                                    </div>
                                    <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Your E-mail Address</div>
                                        <input onChange={(e) => setemail(e.target.value)} type="text" placeholder="E-mail" />
                                    </div>
                                    <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Contact Number</div>
                                        <input onChange={(e) => setcontact(e.target.value)} type="text" placeholder="Contact Number" />
                                    </div>
                                </span>
                            </div>
                        </div>
                        <div onClick={sendEmail} className="submit">
                            SUBMIT
                   </div>
                    </div>
            }

            <Fix_navigation_bar />
            <Navigationbar slide={showMenuBar} />
            <div className="about-sub-nav">
                <Link to="/about" className="inactive about-sub-nav-item">About us</Link>
                <Link to="/vision" className="inactive about-sub-nav-item">Vision</Link>
                <Link to="/mission" className="active about-sub-nav-item">Mission</Link>
                <Link to="/notefromdirector" className="inactive about-sub-nav-item">About Director</Link>
                <Link to="/principlemessage" className="inactive about-sub-nav-item">Principal's Message</Link>
                <Link to="/governingcounsil" className="inactive about-sub-nav-item">Governing Council</Link>



            </div>

            <div className="about-border">
                <div className="about-title" >
                    <strong>MISSION</strong>
                    <div className="about-line"></div>
                </div>
                <p style={{ textAlign: 'justify' }}>
                    We believe architecture to be a conceptually based intellectual endeavour and a form of critical inquiry that addresses the built and natural environments from the scale of the city to the scale
            of the detail.<br /><br />
            The department is committed to producing conceptual thinkers and skilled makers
            who are versed in the techniques and knowledge of the discipline and who are cognizant
             of: critical theory, history, science and progressive social values.<br /><br />
             To realize these objectives, design is taught as a critical, speculative and
             creative endeavour embracing both the humanities and the sciences.<br /><br />
             The College of Architecture strives to use design thinking and creative
             problem solving to address the issues faced by contemporary society.<br /><br />
             We integrate this approach into how we teach our students, the research that
             we produce, and the services that we provide to our communities.<br /><br />
             The overarching vision for the College of Architecture is to prepare our graduates to actively participate in the contemporary environment,
            encourage and anticipate paradigm shifts, and respond to change in the local, national and international communities.


            </p>

            </div>

            <Footer />
        </div>
    )
}

export default Mission
