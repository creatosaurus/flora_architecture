import React, { useState, useEffect } from 'react'
import Footer from '../ReusableComponents/Footer'
import { Link } from 'react-router-dom'
import Info from '../../assets/commerce-and-shopping.svg'
import Social from '../../assets/social.svg'
import { MobilePDFReader } from 'react-read-pdf';
import emailjs from 'emailjs-com';
import ReactModal from 'react-modal';
import Navigation_bar from '../ReusableComponents/Navigation_bar'
import Fix_navigation_bar from '../ReusableComponents/Fix_navigation_bar'
import Slider from '../ReusableComponents/Slider'

const Faculty = () => {
    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])

    const [flag, setflag] = useState(false)
    const [profile, setProfile] = useState(false)
    const [numPages, setnumPages] = useState(null)
    const [pageNumber, setpageNumber] = useState(1)
    const [pdf, setPdf] = useState('Ar Rahuldev Patil_FCOA.docx')
    const [name, setname] = useState("")
    const [email, setemail] = useState("")
    const [contact, setcontact] = useState("")
    const [menu, setmenu] = useState(false)

    const showMenuBar = () => {
        setmenu(!menu)
    }


    const sendEmail = () => {

        var template_params = {
            "name": name,
            "email": email,
            "contact": contact
        }
        emailjs.send('default_service', 'template_QcFeJJkU', template_params, 'user_pMyeJVJzDOA6CDIxE2aM8')
            .then((result) => {
                alert('Form Submitted')
            }, (error) => {
                alert('Please try again')
            });
    }

    const showForm = () => {
        setflag(!flag)
    }
    const showProfile = (file) => {
        setProfile(!profile)

        setPdf(file)
    }

    return (
        <div>
             {
                menu === false ? null
                    : <Slider slide={showMenuBar} />
            }
            {
                flag === false ? <div class="circle" onClick={showForm}>
                    <img src={Info} alt="info" />
                </div> : <div className="form">
                        <div className="head">
                            <div></div>
                            <div>Admission Inquiry</div>
                            <div onClick={showForm} style={{ cursor: 'pointer' }}>X</div>
                        </div>
                        <div className="form-content">
                            <div className="icon-container">
                                <span className="icon">
                                    <img src={Social} alt="social" />
                                </span>
                                <span className="text-content">
                                    Hello! Welcome to Flora college
                              </span>
                            </div>
                            <div className="icon-container1">
                                <span className="icon">
                                    <img src={Social} alt="social" />
                                </span>
                                <span className="text-content" style={{ display: 'flex', flexDirection: 'column' }}>
                                    Please Tell a bit about yourself.
                              <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Your Name</div>
                                        <input onChange={(e) => setname(e.target.value)} type="text" placeholder="full name" />
                                    </div>
                                    <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Your E-mail Address</div>
                                        <input onChange={(e) => setemail(e.target.value)} type="text" placeholder="E-mail" />
                                    </div>
                                    <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Contact Number</div>
                                        <input onChange={(e) => setcontact(e.target.value)} type="text" placeholder="Contact Number" />
                                    </div>
                                </span>
                            </div>
                        </div>
                        <div onClick={sendEmail} className="submit">
                            SUBMIT
                   </div>
                    </div>
            }
            {
                profile === true ? <div style={{ overflow: 'scroll', height: 500 }}>
                    <ReactModal
                        isOpen={profile}
                        contentLabel="onRequestClose Example"
                        onRequestClose={showProfile}
                        shouldCloseOnOverlayClick={true}
                    >
                        <div className="modal-container">
                            <div className="cross" onClick={showProfile}>X</div>
                            <MobilePDFReader url={require(`../../assets/${pdf}`)} />

                        </div>
                    </ReactModal>

                </div>
                    :
                    <div>
                        <Fix_navigation_bar />
                        <Navigation_bar slide={showMenuBar}  />
                        <div className="staff-sub-nav">
                            <Link to="/faculty" className="active staff-sub-nav-item" >Faculty</Link>
                            <Link to="/adminstaff" className="inactive staff-sub-nav-item" >Admin Staff</Link>
                            <Link to="/staffactivity" className="inactive staff-sub-nav-item" >Staff Activity</Link>

                        </div>

                        <div className="faculty-principle-grid">
                            <div className="principle">
                                <div className="admin-principle-title">
                                    <strong >Principal</strong>
                                    <div className="principle-line"></div>
                                </div>
                                <div className="image-container">
                                    <div class="photo-container">
                                        <img class="faculty-image" src={require('../../assets/Rahuldev Patil.jpg')} />
                                    </div>
                                    <div className="admin-principle-name">
                                        Ar. Rahuldev Patil
                           </div>
                                    <div className="faculty-container-designation">
                                        Principal
                           </div>
                                    <div className="faculty-container-qualification">
                                        <p>B. Arch. | Master’s in Landscape Architecture | Post Graduate in Environmental Management</p>
                                    </div>
                                    <div onClick={() => showProfile("Ar Rahuldev Patil_FCOA.pdf")} style={{ margin: 20 }} className="admin-faculty-full-profile-button">
                                        <a className="faculty-full-profile-button-header" href="#profile">Full Profile</a>
                                    </div>
                                </div>
                            </div>
                            <div className="vice-principle">
                                <div className="admin-vice-principle-title">
                                    <strong>Vice Principal</strong>
                                    <div className="principle-line"></div>
                                </div>
                                <div className="image-container">
                                    <div class="photo-container">
                                        <img className="faculty-image" src={require('../../assets/Shahid Rahmat.jpg')} />
                                    </div>
                                    <div className="admin-principle-name">
                                        Dr. Shahid Rahmat
                           </div>
                                    <div className="faculty-container-designation">
                                        Vice Principal
                           </div>
                                    <div className="faculty-container-qualification">
                                        <p>Associate professor - B. Arch. | Master’s in City Planning | Doctorate in  Infrastructure  Planning</p>
                                    </div>
                                    <div onClick={() => showProfile("Profile_Shahid Rahmat.pdf")} style={{ margin: 20 }} className="admin-faculty-full-profile-button">
                                        <a className="faculty-full-profile-button-header" href="#profile">Full Profile</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="faculty-faculty-container">
                            <div className="faculty-header-container" >
                                <div className="faculty-container-header">Faculty</div>
                                <div className="faculty-container-border"></div>
                            </div>
                            <div className="faculty-container-grid">
                                <div className="faculty-conatainer-card">
                                    <div class="photo-container">
                                        <img className="faculty-image" src={require('../../assets/Prashant Joshi.jpg')} alt="" />                                </div>
                                    <div className="admin-principle-name">
                                        Ar. Prathamesh Joshi
                           </div>
                                    <div className="faculty-container-qualification">
                                        Assitant professor
                           </div>
                                    <div className="faculty-container-qualification">
                                        B. Arch. | Master’s in Architectural Project & Construction Managemen
                           </div>
                                    <div onClick={() => showProfile("Prathamesh Joshi.pdf")} className="admin-faculty-full-profile-button">
                                        <a className="faculty-full-profile-button-header" href="#profile" >Full Profile</a>
                                    </div>

                                </div>

                                <div className="faculty-conatainer-card">
                                    <div class="photo-container">
                                        <img className="faculty-image" src={require('../../assets/Shruti Pitale.jpg')} alt="" />
                                    </div>
                                    <div className="admin-principle-name">
                                        Ar. Shruti Pitale
                           </div>
                                    <div className="faculty-container-designation">
                                        Assitant professor
                           </div>
                                    <div className="faculty-container-qualification">
                                        B. Arch. | Master’s in Digital Architecture
                           </div>
                                    <div onClick={() => showProfile("Shruti Pitale.pdf")} style={{ marginTop: 65 }} className="admin-faculty-full-profile-button">
                                        <a className="faculty-full-profile-button-header" href="#profile">Full Profile</a>
                                    </div>

                                </div>



                                <div class="faculty-conatainer-card">
                                    <div class="photo-container">
                                        <img className="faculty-image" src={require('../../assets/Shriram Lele.jpg')} alt="" />
                                    </div>
                                    <div className="admin-principle-name">
                                        Ar. Shriram Lele
                           </div>
                                    <div className="faculty-container-designation">
                                        Assitant professor
                           </div>
                                    <div className="faculty-container-qualification">
                                        B. Arch. | Master’s in City & Regional Planning (Urban Design
                           </div>
                                    <div onClick={() => showProfile("Faculty Profile_FCOA.pdf")} className="admin-faculty-full-profile-button">
                                        <a className="faculty-full-profile-button-header" href="#profile">Full Profile</a>
                                    </div>

                                </div>
                                <div class="faculty-conatainer-card">
                                    <div class="photo-container">

                                        <img className="faculty-image" src={require('../../assets/Rahul Padalkar.jpg')} alt="" />
                                    </div>
                                    <div className="admin-principle-name">
                                        Ar. Rahul Padalkar
                           </div>
                                    <div className="faculty-container-designation">
                                        Assitant professor
                           </div>
                                    <div className="faculty-container-qualification">
                                        BE Civil | Master’s in Transportation Infrastructure and Systems Engineering
                           </div>
                                    <div onClick={() => showProfile("")} className="admin-faculty-full-profile-button">
                                        <a className="faculty-full-profile-button-header" href="#profile">Full Profile</a>
                                    </div>

                                </div>

                                <div class="faculty-conatainer-card">
                                    <div class="photo-container">
                                        <img className="faculty-image" src={require('../../assets/Sanket Khokale.png')} alt="" />
                                    </div>
                                    <div className="admin-principle-name">
                                        Ar. Sanket Khokale
                           </div>
                                    <div className="faculty-container-designation">
                                        Assitant professor
                           </div>
                                    <div className="faculty-container-qualification">
                                        B. Arch. | Master’s in Architectural Conservation
                           </div>
                                    <div onClick={() => showProfile("sanket.pdf")} className="admin-faculty-full-profile-button">
                                        <a className="faculty-full-profile-button-header" href="#profile">Full Profile</a>
                                    </div>

                                </div>
                                <div class="faculty-conatainer-card">
                                    <div class="photo-container">
                                        <img className="faculty-image" src={require('../../assets/Sagar Shinde.jpg')} alt="" />
                                    </div>
                                    <div className="admin-principle-name">
                                        Ar. Sagar Ram Shinde
                           </div>
                                    <div className="faculty-container-designation">
                                        Assitant professor
                           </div>
                                    <div className="faculty-container-qualification">
                                        B. Arch. | Master’s in Architectural Conservation
                           </div>
                                    <div className="admin-faculty-full-profile-button">
                                        <a className="faculty-full-profile-button-header" href="">Full Profile</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="faculty-visiting-container">
                            <div className="faculty-visiting-header-container">
                                <div className="faculty-visiting-container-header">Visiting Faculty</div>
                                <div className="faculty-visiting-header-border"></div>
                            </div>
                            <div className="faculty-visiting-grid">
                                <div className="faculty-visiting-card">
                                <div class="photo-container">
                                    <img className="faculty-image" src={require('../../assets/Shalaka Gotkhindikar.jpg')} alt="" />
                                    </div>
                                    <div className="admin-principle-name">
                                        Ar. Shalaka Gotkhindikar
                           </div>
                                    <div className="faculty-container-designation">
                                        Visiting Faculty
                           </div>
                                    <div className="faculty-container-qualification">
                                        B. Arch. | Master’s in Indology
                           </div>
                                    <div onClick={() => showProfile("shalaka.pdf")} className="admin-faculty-full-profile-button">
                                        <a className="faculty-full-profile-button-header" href="#profile">Full Profile</a>
                                    </div>

                                </div>
                                <div className="faculty-visiting-card">
                                <div class="photo-container">
                                    <img className="faculty-image" src={require('../../assets/Prashant Joshi.jpg')} alt="" />
                                    </div>
                                    <div className="admin-principle-name">
                                        Ar. Prashant Joshi
                           </div>
                                    <div className="faculty-container-designation">
                                        Visiting Faculty
                           </div>
                                    <div className="faculty-container-qualification">
                                        B. Arch. | M. Arch
                           </div>
                                    <div onClick={() => showProfile("Faculty Profile format_FCOA - Prashant Joshi.pdf")} className="admin-faculty-full-profile-button">
                                        <a className="faculty-full-profile-button-header" href="#profile">Full Profile</a>
                                    </div>

                                </div>
                                <div className="faculty-visiting-card">
                                <div class="photo-container">
                                    <img className="faculty-image" src={require('../../assets/Supriya Dhamale.jpg')} alt="" />
                                    </div>
                                    <div className="admin-principle-name">
                                        Ar. Supriya Dhamale
                           </div>
                                    <div className="faculty-container-designation">
                                        Visiting Faculty
                           </div>
                                    <div className="faculty-container-qualification">
                                        B. Arch. |Master’s in Digital Architecture
                           </div>
                                    <div onClick={() => showProfile("Supriya Dhamale_Profile.pdf")} className="admin-faculty-full-profile-button">
                                        <a className="faculty-full-profile-button-header" href="#profile">Full Profile</a>
                                    </div>

                                </div>
                                <div className="faculty-visiting-card">
                                <div class="photo-container">
                                    <img className="faculty-image" src={require('../../assets/Prashant Gadre.jpg')} alt="" />
                                    </div>
                                    <div className="admin-principle-name">
                                        Ar. Prashant Gadre
                           </div>
                                    <div className="faculty-container-designation">
                                        Visiting Faculty
                           </div>
                                    <div className="faculty-container-qualification">
                                        G.D. Arch
                           </div>
                                    <div onClick={() => showProfile("prashant gadre.pdf")} className="admin-faculty-full-profile-button">
                                        <a className="faculty-full-profile-button-header" href="#profile">Full Profile</a>
                                    </div>

                                </div>
                            </div>

                        </div>



                        <Footer />
                    </div>

            }

        </div>
    )
}

export default Faculty
